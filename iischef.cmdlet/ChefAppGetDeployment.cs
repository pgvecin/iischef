﻿using iischef.core;
using System.Linq;
using System.Management.Automation;

namespace iischef.cmdlet
{
    [Cmdlet(VerbsLifecycle.Invoke, "ChefAppGetDeployment")]
    [OutputType(typeof(Deployment))]
    public class ChefAppGetDeployment : Cmdlet
    {
        [Parameter(Position = 1, ValueFromPipelineByPropertyName = true)]
        public string Id { get; set; }

        protected override void ProcessRecord()
        {
            var app = ConsoleUtils.GetApplicationForConsole();
            var installedApplication = app.GetInstalledApps(Id).Single();
            var deployer = app.GetDeployer(installedApplication);
            
            WriteObject(deployer);
        }
    }
}
