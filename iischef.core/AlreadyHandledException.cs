﻿using System;

namespace iischef.core
{
    public class AlreadyHandledException : Exception
    {
        public AlreadyHandledException(string message, Exception inner) : base(message, inner) { }
    }
}
