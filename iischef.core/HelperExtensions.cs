﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iischef.utils;

namespace iischef.core
{
    public static class HelperExtensions
    {
        /// <summary>
        /// Delete the artifact's source if it is remote
        /// </summary>
        /// <param name="artifact"></param>
        public static void DeleteIfRemote(this Artifact artifact)
        {
            if (!artifact.isRemote)
            {
                return;
            }

            UtilsSystem.DeleteDirectory(artifact.localPath);
        }
    }
}
