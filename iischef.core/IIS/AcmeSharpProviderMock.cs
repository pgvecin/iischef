﻿using iischef.logger;
using iischef.utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography.X509Certificates;

namespace iischef.core.IIS
{
    /// <summary>
    /// Mock provider for tests
    /// </summary>
    public class AcmeSharpProviderMock : IAcmeSharpProvider
    {
        protected string Domain;

        protected string ChallengeUrl;

        protected readonly LoggerInterface Logger;

        protected string ChallengeContent;

        public AcmeSharpProviderMock(LoggerInterface logger, string domain)
        {
            this.Logger = logger;
            this.ChallengeContent = Guid.NewGuid().ToString();
            this.Domain = domain;
        }

        /// <inheritdoc cref="IAcmeSharpProvider"/>
        public void InitRegistration(
            string signerPath,
            string registrationPath,
            string email)
        {
            File.WriteAllText(signerPath, string.Empty);
            File.WriteAllText(registrationPath, string.Empty);
        }

        /// <inheritdoc cref="IAcmeSharpProvider"/>
        public void GenerateHttpChallenge(out string challengeUrl, out string challengeContent, out string challengeFilePath)
        {
            challengeFilePath = $".well-known/acme-challenge/" + Guid.NewGuid();
            challengeUrl = $"http://{this.Domain}/" + challengeFilePath;
            this.ChallengeUrl = challengeUrl;
            challengeContent = this.ChallengeContent;
        }

        /// <inheritdoc cref="IAcmeSharpProvider"/>
        public bool ValidateChallenge()
        {
            return string.Equals(UtilsSystem.DownloadUriAsText(this.ChallengeUrl), this.ChallengeContent);
        }

        /// <inheritdoc cref="IAcmeSharpProvider"/>
        public CertificatePaths DownloadCertificate(
            string certificatename,
            string mainhost,
            string certificatePath,
            List<string> alternatehosts = null)
        {
            var result = new CertificatePaths();

            // Creating the self-signed certificate already enrolls it in the local certificate store
            string certificatePassword = null;
            this.Logger.LogInfo(true, "Generating self signed certificate.");
            var cert = UtilsCertificate.CreateSelfSignedCertificate(this.Domain, this.Domain);
            var tmpPfx = Path.GetTempFileName();
            File.WriteAllBytes(tmpPfx, cert.Export(X509ContentType.Pfx, certificatePassword));
            result.pfxPemFile = tmpPfx;

            // Remove the locally signed certificate from the local store, as we are only interested in
            // the exported PFX file
            UtilsCertificate.RemoveCertificateFromLocalStore(cert.Thumbprint);

            return result;
        }

        public void Dispose()
        {
        }
    }
}
