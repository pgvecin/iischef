﻿using System.Collections.Generic;

namespace iischef.core.Php
{
    public class IniFileSection
    {
        public bool IsCommented { get { return _IsCommented; } }

        public IniFileSection(string name, bool isCommented)
        {
            _IsCommented = isCommented;
            this.name = name;
        }

        public string Name { get { return this.name; } }

        private string name;
        private bool _IsCommented;

        public List<IniFileLine> lines = new List<IniFileLine>();
    }
}
