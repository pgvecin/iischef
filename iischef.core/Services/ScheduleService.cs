﻿using iischef.utils;
using Microsoft.Win32.TaskScheduler;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace iischef.core.Services
{
    public class ScheduleService : DeployerBase, DeployerInterface
    {
        /// <summary>
        /// Folder in the scheduler where all the tasks will be stored
        /// </summary>
        protected const string CST_TASK_FOLDER_NAME = "\\Chef";

        /// <summary>
        /// Service typed settings
        /// </summary>
        protected ScheduleServiceSettings Settings => this.DeployerSettings.castTo<ScheduleServiceSettings>();

        /// <inheritdoc cref="DeployerInterface"/>
        public void start()
        {
            using (TaskService ts = new TaskService())
            {
                var task = this.GetTask(ts);
                task.Enabled = this.Settings.disabled != true;
            }
        }

        /// <inheritdoc cref="DeployerInterface"/>
        public void stop()
        {
            using (TaskService ts = new TaskService())
            {
                var task = this.GetTask(ts);

                if (task == null)
                {
                    this.Logger.LogWarning(true, "Could not find scheduler task");
                    return;
                }

                task.Enabled = false;
                this.StopTask(task);
            }
        }

        /// <summary>
        /// Get the instance of the task
        /// </summary>
        /// <param name="ts"></param>
        /// <returns></returns>
        protected Task GetTask(TaskService ts)
        {
            var f = GetFolder(ts);
            var ot = ts.GetTask(f.Path + "\\" + CronId());
            return ot;
        }

        /// <summary>
        /// Get the cron ID for this service
        /// </summary>
        /// <returns></returns>
        protected string CronId()
        {
            var settings = this.DeployerSettings.castTo<ScheduleServiceSettings>();
            return Deployment.shortid + "_" + settings.id; ;
        }

        public void deploySettings(string jsonSettings,
            string jsonSettingsNested,
            RuntimeSettingsReplacer replacer)
        {

        }

        /// <inheritdoc cref="DeployerInterface"/>
        public void undeploy(bool isUninstall = false)
        {
            using (TaskService ts = new TaskService())
            {
                var task = this.GetTask(ts);

                if (task == null)
                {
                    this.Logger.LogWarning(true, "Could not find scheduler task");
                    return;
                }

                task.Enabled = false;
                this.StopTask(task);
                var f = GetFolder(ts);
                f.DeleteTask(task.Name, false);
            }
        }

        protected TaskFolder GetFolder(TaskService ts)
        {
            TaskFolder f = null;

            try
            {
                f = ts.GetFolder(CST_TASK_FOLDER_NAME);
            }
            catch
            {
                // ignored
            }

            return f ?? ts.RootFolder.CreateFolder(CST_TASK_FOLDER_NAME);
        }

        /// <summary>
        /// Stop a task
        /// </summary>
        /// <param name="t"></param>
        protected void StopTask(Task t)
        {
            this.Logger.LogInfo(true, "Stopping scheduler task {0} with state {1}", t.Name, t.State);

            int maxWait = 10000;
            Stopwatch sw = Stopwatch.StartNew();
            sw.Start();

            while (t.State == TaskState.Running && sw.ElapsedMilliseconds < maxWait)
            {
                this.Logger.LogInfo(true, "Waiting for task to stop running...");
                Thread.Sleep(1250);
            }

            // Now send the stop signal
            t.Stop();
        }

        /// <inheritdoc cref="DeployerInterface"/>
        public void deploy()
        {
            var settings = this.Settings;

            string cronId = Deployment.shortid + "_" + settings.id;

            string pwfile = UtilsSystem.CombinePaths(Deployment.runtimePath, "cronjobs_" + settings.id + ".ps1");

            // Necesitamos un Bat que llame al powershel, este siempre tiene el mismo aspecto.
            string batfile = UtilsSystem.CombinePaths(Deployment.runtimePath, "cronjobs_" + settings.id + ".bat");

            Encoding enc = Encoding.GetEncoding("Windows-1252");
            File.WriteAllText(batfile, "powershell " + pwfile, enc);

            StringBuilder command = new StringBuilder();

            // Add path to environment.
            command.AppendLine(
                $"$env:Path = \"{UtilsSystem.CombinePaths(Deployment.runtimePath, "include_path")};\" + $env:Path");

            // Move to runtime.
            command.AppendLine($"cd \"{UtilsSystem.CombinePaths(Deployment.appPath)}\"");

            // Add path of project to the enviroment
            command.AppendLine($"$env:AppPath = \"{UtilsSystem.CombinePaths(Deployment.appPath)}\"");

            // Whatever deployers wanna do...
            var logger = new logger.NullLogger();
            var deployers = Deployment.GrabDeployers(logger);

            foreach (var deployer in deployers)
            {
                deployer.deployConsoleEnvironment(command);
            }

            // Drop the user commands
            if (!string.IsNullOrWhiteSpace(settings.command))
            {
                command.AppendLine(settings.command);
            }

            if (settings.commands != null)
            {
                foreach (var cmd in settings.commands)
                {
                    command.AppendLine(cmd);
                }
            }

            File.WriteAllText(pwfile, command.ToString());

            // Nuestro scheduler tiene un nombre
            // definido.
            using (TaskService ts = new TaskService())
            {
                // Create a new task definition and assign properties
                TaskDefinition td = ts.NewTask();

                // Run with highest level to avoid UAC issues
                // https://www.devopsonwindows.com/create-scheduled-task/
                td.Principal.RunLevel = TaskRunLevel.Highest;

                string password = settings.taskUserPassword;

                if (settings.taskLogonType.HasValue)
                {
                    td.Principal.LogonType = (TaskLogonType)settings.taskLogonType.Value;
                }

                if (settings.taskUserId == "auto")
                {
                    td.Principal.UserId = Deployment.WindowsUsernameFqdn();
                    td.Principal.LogonType = TaskLogonType.Password;
                    password = Deployment.GetWindowsPassword();

                    // Make sure that the user has the LogonAsBatchRight
                    UtilsWindowsAccounts.SetRight(Deployment.WindowsUsernameFqdn(), "SeBatchLogonRight", logger);
                }
                // Default to the SYSTEM account.
                else if (String.IsNullOrWhiteSpace(settings.taskUserId))
                {
                    td.Principal.UserId = "SYSTEM";
                    td.Principal.LogonType = TaskLogonType.ServiceAccount;
                    password = null;
                }

                td.RegistrationInfo.Description = cronId;

                // Create a trigger that will fire the task every 5 minutes.
                var trigger = new DailyTrigger();

                // Habilitada...
                trigger.Enabled = true;

                // Repetir cada 24 horas.
                trigger.DaysInterval = 1;

                // Repetir durante 24 horas en la frecuencia establecida.
                trigger.Repetition = new RepetitionPattern(new TimeSpan(0, settings.frequency, 0), new TimeSpan(24, 0, 0), true);

                // Para que arranque dos minutos después del deploy.
                trigger.StartBoundary = DateTime.Now.AddMinutes(2);

                // Enablin/disabling will happen during start/stop of service
                td.Settings.Enabled = false;

                // Un solo trigger.
                td.Triggers.Add(trigger);

                // Create an action that will launch the bat launcher.
                td.Actions.Add(new ExecAction(batfile, null, null));

                TaskFolder f = GetFolder(ts);

                // Register the task in the root folder
                if (!string.IsNullOrWhiteSpace(password) && td.Principal.LogonType == TaskLogonType.Password)
                {
                    f.RegisterTaskDefinition(td.RegistrationInfo.Description, td, TaskCreation.Create, td.Principal.UserId, Deployment.GetWindowsPassword(), td.Principal.LogonType);
                }
                else
                {
                    f.RegisterTaskDefinition(td.RegistrationInfo.Description, td, TaskCreation.Create, td.Principal.UserId);
                }
            }
        }

        /// <inheritdoc cref="DeployerInterface"/>
        public override void cleanup()
        {
            using (TaskService ts = new TaskService())
            {
                var f = GetFolder(ts);

                foreach (var t in f.AllTasks.ToList())
                {
                    // If task does not belong to our app skip
                    if (!t.Name.StartsWith(this.Deployment.GetShortIdPrefix()))
                    {
                        continue;
                    }

                    // If task is from this deployment, skip
                    if (t.Name.StartsWith(this.Deployment.shortid))
                    {
                        continue;
                    }

                    this.Logger.LogWarning(true, "Removed stuck scheduler task {0}", t.Name);
                    f.DeleteTask(t.Name);
                }
            }
        }

        /// <inheritdoc cref="DeployerInterface"/>
        public void sync()
        {
        }
    }
}
