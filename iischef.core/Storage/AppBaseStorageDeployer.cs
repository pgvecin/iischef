﻿using iischef.utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Security.Principal;

namespace iischef.core.Storage
{
    public class AppBaseStorageDeployer : DeployerBase, DeployerInterface
    {
        public const string LEGACY_CHEF_USERS_GROUPNAME = "chef_users";

        /// <summary>
        /// List of configuration files were the runtime
        /// settings will be replaced.
        /// </summary>
        /// <returns></returns>
        public AppBaseStorageDeployerSettings GetSettings()
        {
            return (AppBaseStorageDeployerSettings)this.DeployerSettings.ToObject(typeof(AppBaseStorageDeployerSettings));
        }

        /// <inheritdoc cref="DeployerBase"/>
        public void deploy()
        {
            var settings = this.GetSettings();

            Deployment.windowsUsername = "chf_" + Deployment.installedApplicationSettings.GetId();

            if (Deployment.GetPreviousDeployment() != null && Deployment.GetPreviousDeployment().windowsUsername != Deployment.windowsUsername)
            {
                this.Logger.LogWarning(
                    false,
                    "Windows account username has changed from '{0}' to '{1}', removal of account and granted permissions must be performed manually.",
                    Deployment.GetPreviousDeployment()?.windowsUsername,
                    Deployment.windowsUsername);
            }

            UtilsWindowsAccounts.EnsureUserExists(Deployment.WindowsUsernameFqdn(), Deployment.GetWindowsPassword(), Deployment.installedApplicationSettings.GetId(), this.Logger, this.GlobalSettings.directoryPrincipal);

            // Legacy behaviour, if no userGroups defined, create a chef_users groups and add the users
            // to it
            if (!(GlobalSettings.userGroups ?? new List<string>()).Any())
            {
                UtilsWindowsAccounts.EnsureGroupExists(LEGACY_CHEF_USERS_GROUPNAME, this.GlobalSettings.directoryPrincipal);
                UtilsWindowsAccounts.EnsureUserInGroup(Deployment.WindowsUsernameFqdn(), LEGACY_CHEF_USERS_GROUPNAME, this.Logger, this.GlobalSettings.directoryPrincipal);
            }

            // Add the user to the user groups
            foreach (var groupIdentifier in GlobalSettings.userGroups ?? new List<string>())
            {
                UtilsWindowsAccounts.EnsureUserInGroup(Deployment.WindowsUsernameFqdn(), groupIdentifier, this.Logger, this.GlobalSettings.directoryPrincipal);
            }

            // Add the user to any user groups defined at the application level
            foreach (var groupIdentifier in settings.user_groups ?? new List<string>())
            {
                UtilsWindowsAccounts.EnsureUserInGroup(Deployment.WindowsUsernameFqdn(), groupIdentifier, this.Logger, this.GlobalSettings.directoryPrincipal);
            }

            // Add any privileges if requested
            foreach (var privilegeName in settings.privileges ?? new List<string>())
            {
                UtilsWindowsAccounts.SetRight(Deployment.WindowsUsernameFqdn(), privilegeName, this.Logger);
            }

            // Getting security right at the OS level here is a little bit picky...
            // in order to have REALPATH to work in PHP we need to be able to read all directories
            // in a path i.e. D:\webs\chef\appnumber1\
            // What we will do is disconnect the USERS account here...
            string basePath = UtilsSystem.CombinePaths(GlobalSettings.GetDefaultApplicationStorage().path, Deployment.getShortId());
            UtilsSystem.EnsureDirectoryExists(basePath, true);

            UtilsWindowsAccounts.DisablePermissionInheritance(basePath);
            UtilsWindowsAccounts.RemoveAccessRulesForIdentity(new SecurityIdentifier(UtilsWindowsAccounts.WELL_KNOWN_SID_USERS), basePath, this.Logger);
            UtilsWindowsAccounts.AddPermissionToDirectoryIfMissing(Deployment.WindowsUsernameFqdn(), basePath, FileSystemRights.ReadAndExecute, this.GlobalSettings.directoryPrincipal);

            // Store this in the application storage location.
            Deployment.runtimePath = UtilsSystem.CombinePaths(basePath, "runtime");
            UtilsSystem.EnsureDirectoryExists(Deployment.runtimePath, true);

            Deployment.runtimePathWritable = UtilsSystem.CombinePaths(basePath, "runtime_writable");
            UtilsSystem.EnsureDirectoryExists(Deployment.runtimePathWritable, true);

            // Due to compatibility reasons with environments such as PHP (that do not play well with network file URIs such as shared folders)
            // by default these two directories are symlinked to a local path if they are network paths.

            // Temp dir
            string localTempPath = UtilsSystem.CombinePaths(Deployment.runtimePath, "temp");
            string remoteTempPath = UtilsSystem.CombinePaths(GlobalSettings.GetDefaultTempStorage().path, Deployment.installedApplicationSettings.GetId());
            UtilsSystem.EnsureDirectoryExists(remoteTempPath, true);
            UtilsJunction.EnsureLink(localTempPath, remoteTempPath, this.Logger, false);
            Deployment.tempPath = localTempPath;

            // Temp dir sys
            Deployment.tempPathSys = UtilsSystem.CombinePaths(Deployment.runtimePathWritable, "_tmp"); ;
            UtilsSystem.EnsureDirectoryExists(Deployment.tempPathSys, true);

            // Log dir
            string localLogPath = UtilsSystem.CombinePaths(Deployment.runtimePath, "log");
            string remoteLogPath = UtilsSystem.CombinePaths(GlobalSettings.GetDefaultLogStorage().path, Deployment.installedApplicationSettings.GetId());
            UtilsSystem.EnsureDirectoryExists(remoteLogPath, true);
            UtilsJunction.EnsureLink(localLogPath, remoteLogPath, this.Logger, false);
            Deployment.logPath = localLogPath;

            Deployment.SetSetting("appstorage.base", basePath);
            Deployment.SetSetting("appstorage.temp", Deployment.tempPath);
            Deployment.SetSetting("appstorage.log", Deployment.logPath);

            // We use this flag to detect transient storage
            // that must be removed when the deployer is "undeployed".
            AppBaseStorageType appBaseStorageType = AppBaseStorageType.Original;

            // TODO: Make this configurable through the chef.yml settings file.
            string ignoreOnDeployPattern = "^\\.git\\\\|^chef\\\\|^\\.vs\\\\";

            switch (Deployment.installedApplicationSettings.GetApplicationMountStrategy())
            {
                case ApplicationMountStrategy.Copy:
                    Deployment.appPath = UtilsSystem.CombinePaths(basePath, "app");
                    // TODO: We should consider the ability to symlink the code here, or to point/mount directly
                    // to the original source path. This would probably require delegating this step to the artifact downloader
                    // (artifact.getDownloader()) or having the downloader tell us how to deal with this (symlinks, direct, whatever)
                    this.Logger.LogInfo(true, "Copying artifact files...");
                    UtilsSystem.CopyFilesRecursivelyFast(Deployment.artifact.localPath, Deployment.appPath, false, ignoreOnDeployPattern, this.Logger);
                    Logger.LogInfo(true, "Ensure app has proper user permissions for account '{0}'", Deployment.WindowsUsernameFqdn());
                    UtilsWindowsAccounts.AddPermissionToDirectoryIfMissing(Deployment.WindowsUserPrincipalName(), Deployment.appPath, FileSystemRights.ReadAndExecute, this.GlobalSettings.directoryPrincipal);
                    Deployment.artifact.DeleteIfRemote();
                    appBaseStorageType = AppBaseStorageType.Transient;
                    break;
                case ApplicationMountStrategy.Move:
                    Deployment.appPath = UtilsSystem.CombinePaths(basePath, "app");
                    // TODO: We should consider the ability to symlink the code here, or to point/mount directly
                    // to the original source path. This would probably require delegating this step to the artifact downloader
                    // (artifact.getDownloader()) or having the downloader tell us how to deal with this (symlinks, direct, whatever)
                    this.Logger.LogInfo(true, "Moving artifact files...");
                    UtilsSystem.MoveDirectory(Deployment.artifact.localPath, Deployment.appPath, this.Logger, ignoreOnDeployPattern);
                    // We had issues in appveyor where _webs location is in C drive and thus not giving
                    // permissions here would make tests fail.
                    Logger.LogInfo(true, "Ensure app has proper user permissions for account '{0}'", Deployment.WindowsUsernameFqdn());
                    UtilsWindowsAccounts.AddPermissionToDirectoryIfMissing(Deployment.WindowsUsernameFqdn(), Deployment.appPath, FileSystemRights.ReadAndExecute, this.GlobalSettings.directoryPrincipal);
                    Deployment.artifact.DeleteIfRemote();
                    appBaseStorageType = AppBaseStorageType.Transient;
                    break;
                case ApplicationMountStrategy.Link:
                    this.Logger.LogInfo(true, "Linking artifact files...");
                    Deployment.appPath = UtilsSystem.CombinePaths(basePath, "app");
                    UtilsJunction.EnsureLink(Deployment.appPath, Deployment.artifact.localPath, this.Logger, false);
                    Logger.LogInfo(true, "Ensure app has proper user permissions for account '{0}'", Deployment.WindowsUsernameFqdn());
                    UtilsWindowsAccounts.AddPermissionToDirectoryIfMissing(Deployment.WindowsUsernameFqdn(), Deployment.artifact.localPath, FileSystemRights.ReadAndExecute, this.GlobalSettings.directoryPrincipal);
                    appBaseStorageType = AppBaseStorageType.Symlink;
                    break;
                case ApplicationMountStrategy.Original:
                    Logger.LogInfo(true, "Ensure app has proper user permissions for account '{0}'", Deployment.WindowsUsernameFqdn());
                    UtilsWindowsAccounts.AddPermissionToDirectoryIfMissing(Deployment.WindowsUsernameFqdn(), Deployment.artifact.localPath, FileSystemRights.ReadAndExecute, this.GlobalSettings.directoryPrincipal);
                    Deployment.appPath = UtilsSystem.CombinePaths(Deployment.artifact.localPath);
                    appBaseStorageType = AppBaseStorageType.Original;
                    break;
                default:
                    throw new NotImplementedException("The requested mount strategy for the application is not available: " + Deployment.installedApplicationSettings.GetApplicationMountStrategy());
            }

            Deployment.SetRuntimeSetting("deployment.appPath", Deployment.appPath);
            Deployment.SetRuntimeSetting("deployment.logPath", Deployment.logPath);
            Deployment.SetRuntimeSetting("deployment.tempPath", Deployment.tempPath);

            Deployment.SetSetting("appstorage.appBaseStorageType", appBaseStorageType);

            UtilsWindowsAccounts.AddPermissionToDirectoryIfMissing(Deployment.WindowsUsernameFqdn(), remoteTempPath, FileSystemRights.Write | FileSystemRights.Read | FileSystemRights.Delete, this.GlobalSettings.directoryPrincipal);
            UtilsWindowsAccounts.AddPermissionToDirectoryIfMissing(Deployment.WindowsUsernameFqdn(), remoteLogPath, FileSystemRights.Write | FileSystemRights.Read | FileSystemRights.Delete, this.GlobalSettings.directoryPrincipal);
            UtilsWindowsAccounts.AddPermissionToDirectoryIfMissing(Deployment.WindowsUsernameFqdn(), Deployment.runtimePath, FileSystemRights.ReadAndExecute, this.GlobalSettings.directoryPrincipal);
            UtilsWindowsAccounts.AddPermissionToDirectoryIfMissing(Deployment.WindowsUsernameFqdn(), Deployment.runtimePathWritable, FileSystemRights.Write | FileSystemRights.Read | FileSystemRights.Delete, this.GlobalSettings.directoryPrincipal);
        }

        public void undeploy(bool isUninstall = false)
        {
            if (Deployment == null)
            {
                return;
            }

            AppBaseStorageType appBaseStorageType = Deployment.GetSetting<AppBaseStorageType>(
                "appstorage.appBaseStorageType",
                AppBaseStorageType.Original,
                this.Logger,
                true);

            string appStorageBasePath = Deployment.GetSetting<string>("appstorage.base", null, this.Logger);

            switch (appBaseStorageType)
            {
                case AppBaseStorageType.Transient:
                    UtilsSystem.DeleteDirectory2(appStorageBasePath, false, 20, this.Logger);
                    break;
                case AppBaseStorageType.Symlink:
                    UtilsJunction.RemoveJunction(Deployment.appPath);
                    UtilsSystem.DeleteDirectory2(appStorageBasePath, false, 20, this.Logger);
                    break;
                case AppBaseStorageType.Original:
                    // Do nothing.
                    break;
                default:
                    throw new Exception("Option not supported.");
            }

            if (!isUninstall)
            {
                return;
            }

            var canonicalPath = Deployment.GetSetting<string>("appstorage.canonical", null, this.Logger);
            if (Directory.Exists(canonicalPath) && UtilsJunction.Exists(canonicalPath))
            {
                Directory.Delete(canonicalPath);
            }

            // Usually the IIS site has been closed a few fractions of a second
            // before this is called, so the folders have probably not yet
            // been released, waitPauseMs at least 10 seconds.
            UtilsSystem.DeleteDirectory(Deployment.GetSetting<string>("appstorage.temp", null, this.Logger), true, 10);
            UtilsSystem.DeleteDirectory(Deployment.GetSetting<string>("appstorage.log", null, this.Logger), true, 10);

            var settings = this.GetSettings();

            var groups = this.GlobalSettings.userGroups ?? new List<string>();

            // add legacy group chef_users
            groups.Add(LEGACY_CHEF_USERS_GROUPNAME);

            // Remove user from all grups before deleting
            foreach (var groupIdentifier in groups)
            {
                UtilsWindowsAccounts.EnsureUserNotInGroup(Deployment.WindowsUsernameFqdn(), groupIdentifier, this.Logger, this.GlobalSettings.directoryPrincipal);
            }

            // Add the user to any user groups defined at the application level
            foreach (var groupIdentifier in settings.user_groups ?? new List<string>())
            {
                UtilsWindowsAccounts.EnsureUserNotInGroup(Deployment.WindowsUsernameFqdn(), groupIdentifier, this.Logger, this.GlobalSettings.directoryPrincipal);
            }

            UtilsWindowsAccounts.DeleteUser(Deployment.WindowsUsernameFqdn(), this.GlobalSettings.directoryPrincipal);
        }

        /// <summary>
        /// Deploy the application runtime settings.
        /// </summary>
        /// <param name="jsonSettings"></param>
        /// <param name="jsonSettingsArray"></param>
        /// <param name="replacer"></param>
        public void deploySettings(string jsonSettings,
            string jsonSettingsArray,
            RuntimeSettingsReplacer replacer)
        {
            // Write the settings in a directory in the application folder itself.
            // When deployed as web app or similar, the other deployers must hide this directory...
            var settingsFile = UtilsSystem.EnsureDirectoryExists(
                Path.Combine(Deployment.runtimePath, "chef-settings.json"));

            File.WriteAllText(settingsFile, jsonSettings);

            var settingsFileNested = UtilsSystem.EnsureDirectoryExists(
                Path.Combine(Deployment.runtimePath, "chef-settings-nested.json"));

            File.WriteAllText(settingsFileNested, jsonSettingsArray);

            // Why don't we write the settings directly to the AppRoot? Because it might
            // be exposed to the public if the application is mounted as a web application...
            // So we just hint to the location of the runtime, and the application
            // must implement the code needed to load the settings.
            var hintFile = UtilsSystem.EnsureDirectoryExists(
                UtilsSystem.CombinePaths(Deployment.appPath, "chef-runtime.path"));

            // We hint to the runtime path, not the specific file
            File.WriteAllText(hintFile, Deployment.runtimePath);

            // Dump the configuration files if requested to do so...
            foreach (var kvp in this.GetSettings().configuration_dump_paths ?? new Dictionary<string, string>())
            {
                var destinationDir = UtilsSystem.CombinePaths(Deployment.appPath, kvp.Value);
                if (!Directory.Exists(destinationDir))
                {
                    Directory.CreateDirectory(destinationDir);
                }

                var settingsFileDump = UtilsSystem.EnsureDirectoryExists(
                Path.Combine(destinationDir, "chef-settings.json"));

                File.WriteAllText(settingsFileDump, jsonSettings);

                var settingsFileNestedDump = UtilsSystem.EnsureDirectoryExists(
                    Path.Combine(destinationDir, "chef-settings-nested.json"));

                File.WriteAllText(settingsFileNestedDump, jsonSettingsArray);

                var settingsFileNestedYaml = UtilsSystem.EnsureDirectoryExists(
                    Path.Combine(destinationDir, "chef-settings-nested.yml"));

                File.WriteAllText(settingsFileNestedYaml, UtilsYaml.JsonToYaml(jsonSettingsArray));
            }

            // Now replace the settings in the configuration templates
            foreach (var kvp in this.GetSettings().configuration_replacement_files ?? new Dictionary<string, string>())
            {
                var sourcePath = UtilsSystem.CombinePaths(Deployment.appPath, kvp.Key);
                var destinationPath = UtilsSystem.CombinePaths(Deployment.appPath, kvp.Value);

                var contents = File.ReadAllText(sourcePath);

                if (destinationPath == sourcePath)
                {
                    throw new Exception("Destination and source for configuration settings replacements cannot be the same.");
                }

                contents = replacer.DoReplace(contents);

                File.WriteAllText(destinationPath, contents);
            }
        }

        public override void beforeDone()
        {
            // We also have a canonical access to the deployed app through a symlink
            string basePath = Deployment.GetSetting("appstorage.base", (string)null, this.Logger);
            string canonicalPath = UtilsSystem.CombinePaths(GlobalSettings.GetDefaultApplicationStorage().path, "_" + Deployment.installedApplicationSettings.GetId());
            UtilsJunction.EnsureLink(canonicalPath, basePath, this.Logger, false, true);
            Deployment.SetSetting("appstorage.canonical", canonicalPath);
        }

        public void start()
        {

        }

        public void stop()
        {
            if (Deployment == null)
            {
                return;
            }
        }

        public void sync()
        {
        }

        public override void done()
        {
            this.Logger.LogInfo(true, "Clearing File System Cache..." + Environment.NewLine + Environment.NewLine + UtilsSystem.DebugTable(FileSystemCache.GetFileSystemCacheBytes()));

            FileSystemCache.ClearFileSystemCache(true);

            this.Logger.LogInfo(true, "Finished clearing File System Cache..." + Environment.NewLine + Environment.NewLine + UtilsSystem.DebugTable(FileSystemCache.GetFileSystemCacheBytes()));
        }
    }
}
