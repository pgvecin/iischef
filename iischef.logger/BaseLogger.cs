﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Text;

namespace iischef.logger
{
    public abstract class BaseLogger : LoggerInterface
    {
        protected bool Verbose = false;

        /// <summary>
        /// Enabel or disable verbose mode for the logger.
        /// </summary>
        /// <param name="verbose"></param>
        public void SetVerbose(bool verbose)
        {
            this.Verbose = verbose;
        }

        protected void WriteEntry(bool verbose, string content, object[] replacements, EventLogEntryType type)
        {
            if (verbose && !this.Verbose)
            {
                return;
            }

            string data = content;

            try
            {
                data = string.Format(content, replacements);
                data = data
                    .TrimEnd(Environment.NewLine.ToCharArray())
                    .TrimStart(Environment.NewLine.ToCharArray());
            }
            catch
            {
                // ignored
            }

            DoWrite(data, type);
        }

        protected abstract void DoWrite(string content, EventLogEntryType type);

        /// <summary>
        /// Everything we log in this monitor application
        /// is an error.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="replacements"></param>
        public void LogError(string message, params object[] replacements)
        {
            WriteEntry(false, message, replacements, EventLogEntryType.Error);
        }

        public void LogInfo(bool verbose, string message, params object[] replacements)
        {
            WriteEntry(verbose, message, replacements, EventLogEntryType.Information);
        }

        public void LogWarning(bool verbose, string message, params object[] replacements)
        {
            WriteEntry(verbose, message, replacements, EventLogEntryType.Warning);
        }

        public void LogException(Exception e, EventLogEntryType entryType = EventLogEntryType.Error)
        {
            StringBuilder sb = new StringBuilder();
            DumpException(e, sb);
            // Exceptions are never verbose, and this is important information that needs to be recored in the logs!
            WriteEntry(false, sb.ToString(), null, entryType);
        }

        /// <summary>
        /// Make sure that relevant details are extract from exceptions!
        /// </summary>
        /// <param name="e"></param>
        /// <param name="sb"></param>
        protected void DumpException(Exception e, StringBuilder sb)
        {
            if (sb == null)
            {
                sb = new StringBuilder();
            }

            sb.AppendLine("Exception **************");
            sb.AppendLine("Exception HResult: '" + Convert.ToString((uint)e.HResult) + "'");
            sb.AppendLine("Exception Type: '" + e.GetType().FullName);
            sb.AppendLine("ExceptionMessage: " + e.Message);
            sb.AppendLine("Stack Trace: " + e.StackTrace);

            if (e is ReflectionTypeLoadException reflectionTypeLoadException)
            {
                foreach (var loaderException in reflectionTypeLoadException.LoaderExceptions)
                {
                    sb.AppendLine("-- Loader exception:" + loaderException.Message);
                    if (loaderException.InnerException != null)
                    {
                        DumpException(loaderException.InnerException, sb);
                    }
                }
            }

            if (e is BadImageFormatException badImageFormatException)
            {
                sb.AppendLine("FileName: " + badImageFormatException.FileName);
                sb.AppendLine("FusionLog: " + badImageFormatException.FusionLog);
                sb.AppendLine("Environment.Is64BitProcess: " + (Environment.Is64BitProcess ? "yes" : "no"));
            }

            if (e is FileNotFoundException fileNotFoundException)
            {
                sb.AppendLine("FusionLog: " + fileNotFoundException.FusionLog);
                sb.AppendLine("FileName: " + fileNotFoundException.FileName);
            }

            if (e.InnerException != null)
            {
                DumpException(e.InnerException, sb);
            }
        }
    }
}
