﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Security.Cryptography;
using System.Security.Principal;
using System.IO;
using System.Net;

namespace iischef.logger
{
    /// <summary>
    /// Logger (system log) for the application.
    /// </summary>
    public class NullLogger : BaseLogger, LoggerInterface
    {

        public NullLogger()
        {


        }

        protected override void DoWrite(string content, System.Diagnostics.EventLogEntryType type)
        {
            
        }
    }
}
