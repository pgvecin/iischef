﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Security.Cryptography;
using System.Security.Principal;
using System.IO;
using System.Net;

namespace iischef.logger
{
    /// <summary>
    /// Logger (system log) for the application.
    /// </summary>
    public class SystemLogger : BaseLogger, LoggerInterface
    {
        protected EventLog log;

        /// <summary>
        /// Start a system based logger.
        /// </summary>
        /// <param name="name">Name for the service log group</param>
        public SystemLogger(string sLog, string sSource = null)
        {
            // None of these should have the SAME name
            // as the service otherwise you are screwed
            // when deploying.
            if (sSource == null)
                sSource = sLog;

            log = new EventLog(sLog);
            log.Source = sSource;

            if (!EventLog.SourceExists(sSource))
            {
                EventLog.CreateEventSource(sSource, sLog);
            }
        }

        protected override void DoWrite(string content, System.Diagnostics.EventLogEntryType type)
        {
            log.WriteEntry(content, type);
        }
    }
}
