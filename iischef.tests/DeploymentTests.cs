﻿using iischef.core;
using iischef.utils;
using Microsoft.Web.Administration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using Xunit;
using Application = iischef.core.Application;

namespace healthmonitortests
{
    /// <summary>
    /// 
    /// </summary>
    public class BasicTests : IClassFixture<ChefTestFixture>
    {
        /// <summary>
        /// Get an instance of BasicTests
        /// </summary>
        /// <param name="fixture"></param>
        public BasicTests(ChefTestFixture fixture)
        {
        }

        /// <summary>
        /// Test that a deployment from AppVeyor works
        /// </summary>
        [Fact]
        [Trait("Category", "Core")]
        public void TestAppVeyorBasedDeployment()
        {
            var logger = new iischef.logger.SystemLogger("Application");

            // Prepare a local artifact from path
            string dir = UtilsSystem.GetTempPath("iischeftest");

            if (Directory.Exists(dir))
            {
                Directory.Delete(dir, true);
            }

            string installedAppConfiguration = UtilsSystem.GetResourceFileAsString("samples/sample-app.yml");

            // Let's grab the
            var tempSettings = UtilsSystem.EnsureDirectoryExists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\iischef\\server-settings.json");
            File.WriteAllText(tempSettings, UtilsSystem.GetResourceFileAsString("samples/server-settings.json"));

            var app = new Application(logger);
            app.Initialize(tempSettings, "testenvironment");

            app.DeploySingleAppFromTextSettings(installedAppConfiguration);
            app.UndeploySingleApp(installedAppConfiguration);
        }

        /// <summary>
        /// Test that a deployment with expiration expires properly
        /// </summary>
        [Fact]
        [Trait("Category", "Core")]
        public void TestAppVeyorBasedDeploymentExpires()
        {
            const string appId = "auto-chef-appveyor-sample-expires";
            Assert.True(appId.StartsWith(Application.AutoDeployApplicationIdPrefix), "Emulates an automated deployment.");

            var logger = new iischef.logger.SystemLogger("Application");

            // Prepare a local artifact from path
            string dir = UtilsSystem.GetTempPath("iischeftest");

            if (Directory.Exists(dir))
            {
                Directory.Delete(dir, true);
            }

            string installedAppConfiguration = UtilsSystem.GetResourceFileAsString("samples/sample-app-expires.yml");

            // Let's grab the
            var tempSettings = UtilsSystem.EnsureDirectoryExists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\iischef\\server-settings.json");
            File.WriteAllText(tempSettings, UtilsSystem.GetResourceFileAsString("samples/server-settings.json"));

            var app = new Application(logger);
            app.Initialize(tempSettings, "testenvironment");

            app.DeploySingleAppFromTextSettings(installedAppConfiguration);

            string appBaseStoragePath = app.GetDeployer(app.GetInstalledApp(appId))
                .DeploymentActive.appPath;

            Assert.True(Directory.Exists(appBaseStoragePath));

            // Should not be removed
            app.RemoveExpiredApplications(DateTime.UtcNow.ToUnixTimestamp());
            Assert.True(app.GetInstalledApp(appId) != null);

            // Should not be removed
            app.RemoveExpiredApplications(DateTime.UtcNow.AddHours(15).ToUnixTimestamp());
            Assert.True(app.GetInstalledApp(appId) != null);

            // Should be removed
            app.RemoveExpiredApplications(DateTime.UtcNow.AddHours(26).ToUnixTimestamp());
            Assert.True(app.GetInstalledApp(appId) == null);

            // Extra test... make sure that the base application storage is CLEARED!
            Assert.False(Directory.Exists(appBaseStoragePath));
        }

        protected void AssertStringDoesNotContains(string needle, string haystack)
        {

            Assert.False(haystack.Contains(needle),
                $"Expected to find '{needle}' in '{haystack}'");

        }

        protected void AssertStringContains(string needle, string haystack)
        {

            Assert.True(haystack.Contains(needle),
                $"Expected to find '{needle}' in '{haystack}'");

        }

        protected void AssertStringEquals(string str1, string str2)
        {
            Assert.True(str1 == str2,
                $"Expected to '{str1}' to equal '{str2}'");
        }

        /// <summary>
        /// Assert hat a URI call response has a specific text.
        /// </summary>
        /// <param name="uri"></param>
        /// <param name="doesContains"></param>
        /// <param name="doesNotContains"></param>
        /// <param name="httpStatusCode"></param>
        protected void AssertUriContains(string uri, List<string> doesContains, List<string> doesNotContains, HttpStatusCode httpStatusCode = HttpStatusCode.OK)
        {
            string response = UtilsSystem.DownloadUriAsText(uri, true, httpStatusCode);

            if (doesContains != null)
            {
                foreach (var c in doesContains)
                {
                    AssertStringContains(c, response);
                }
            }

            if (doesNotContains != null)
            {
                foreach (var c in doesNotContains)
                {
                    AssertStringDoesNotContains(c, response);
                }
            }
        }

        private static void DoIisReset()
        {
            Process iisReset = new Process();
            iisReset.StartInfo.FileName = "iisreset.exe";
            iisReset.StartInfo.RedirectStandardOutput = true;
            iisReset.StartInfo.UseShellExecute = false;
            iisReset.Start();
            iisReset.WaitForExit();
            System.Threading.Thread.Sleep(500);
        }

        [Fact]
        [Trait("Category", "Core")]
        public void TestDeployPhpRuntimeV1_Link()
        {
            _TestDeployPhpRuntimeV1("link");
        }

        [Fact]
        [Trait("Category", "Core")]
        public void TestDeployPhpRuntimeV1_Copy()
        {
            _TestDeployPhpRuntimeV1("copy");
        }

        [Fact]
        [Trait("Category", "Core")]
        public void TestDeployPhpRuntimeV1_Move()
        {
            _TestDeployPhpRuntimeV1("move");
        }

        [Fact]
        [Trait("Category", "Core")]
        public void TestDeployPhpRuntimeV1_Original()
        {
            _TestDeployPhpRuntimeV1("original");
        }

        /// <summary>
        /// El deployer de IIS tiene unos flaws históricos
        /// </summary>
        [Fact]
        [Trait("Category", "Core")]
        public void TestBrokenIisRedeploy()
        {
            var mountstrategy = "link";

            DoIisReset();

            var logger = new iischef.logger.SystemLogger("Application");

            // Prepare a local artifact from path
            string dir = UtilsSystem.GetTempPath("iischeftest1");

            UtilsSystem.DeleteDirectory(dir);

            var chefv1 = UtilsSystem.GetResourceFileAsPath("samples/chef.yml");
            var sampleArtifact = UtilsSystem.GetResourceFileAsPath("samples/sample-php-artifact.zip");


            ZipFile.ExtractToDirectory(sampleArtifact, dir);
            File.Copy(chefv1, UtilsSystem.EnsureDirectoryExists(UtilsSystem.CombinePaths(dir, "chef", "chef.yml")), true);

            // Let's grab the
            var tempSettings = UtilsSystem.EnsureDirectoryExists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\iischef\\server-settings.json");
            File.WriteAllText(tempSettings, UtilsSystem.GetResourceFileAsString("samples/server-settings.json"));

            var app = new Application(logger);
            app.Initialize(tempSettings, "testenvironment");
            app.UseParentLogger();

            string installedAppConfiguration = null;

            installedAppConfiguration = UtilsSystem.GetResourceFileAsString("samples/sample-app-local.yml");
            installedAppConfiguration = installedAppConfiguration.Replace("%PATH%", dir);
            installedAppConfiguration = installedAppConfiguration.Replace("%MOUNTSTRATEGY%", mountstrategy);

            Deployment deployment;

            deployment = app.DeploySingleAppFromTextSettings(installedAppConfiguration);

            Action<string> testContains = (string uri) =>
            {
                AssertUriContains(uri, new List<string>() {
                        "Hello world!",
                        "deployment.custom.setting",
                        "mycustomserverleveloverride",
                        "my_custom_setting",
                        "services.sqlsrv.username",
                        "services.sqlsrv.password",
                        "services.sqlsrv.database",
                        "services.sqlsrv.host",
                        "\"deployment.artifact.branch\": \"mainbranch\"",
                        "\"deployment.artifact.commit_sha\": \"5a1efde8b452e7a385592aa98a93066aa20282e5\"",
                        "services.couchbase-cache.uri",
                        "services.couchbase-cache.bucket-name",
                        "services.couchbase-cache.bucket-password"
                    }, null);
            };

            Action<string> testContains2 = (string uri) =>
            {
                AssertUriContains(uri, new List<string>() {
                        "e:/whatisthis/whatitis",
                        "e:/whatisthis/whatitis/",
                        "preferred_prefix"
                    }, null);
            };

            try
            {
                testContains2("http://chef.testing.framework/config.php");

                testContains("http://chef.testing.framework");
                testContains("http://chef.testing.framework.private");
                testContains("http://local.chefcdn.com/mytestapplicationdirectory");
                testContains("http://local.chefcdn.com/mytestapplicationdirectory/");

                var settings = app.GetGlobalSettings();
                AssertStringEquals("myusername", settings.accounts.First().username);

                // Let's delete directly the IIS site. What will happen is that during redeploy
                // we will get a SiteId for the new deployment that is already in-use by 
                // the previous deployment, so we have a conflict there that needs to 
                // be dealt with.

                string siteName = deployment.getShortId(); // "chf_php-test";

                using (ServerManager manager = new ServerManager())
                {
                    // Deploy the site, ideally NO site should be found
                    // here as it indicates a failed previous deployment.
                    var site = (from p in manager.Sites
                                where p.Name == siteName
                                select p).SingleOrDefault();

                    manager.Sites.Remove(site);

                    UtilsIis.CommitChanges(manager);
                }

                app.RedeployInstalledApplication(false, deployment.installedApplicationSettings.GetId(), true);

                testContains("http://chef.testing.framework");
                testContains("http://chef.testing.framework.private");
                testContains("http://local.chefcdn.com/mytestapplicationdirectory");
                testContains("http://local.chefcdn.com/mytestapplicationdirectory/");
            }
            finally
            {
                app.RemoveAppById(deployment.installedApplicationSettings.GetId());

                // If the folder was moved, nothing to delete...
                if (mountstrategy != "move")
                {
                    UtilsSystem.DeleteDirectory(dir, true);
                }
            }
        }

        /// <summary>
        /// El deployer de IIS tiene unos flaws históricos
        /// </summary>
        [Fact]
        [Trait("Category", "Core")]
        public void TestSiteOfflinePage()
        {
            var mountstrategy = "link";

            DoIisReset();

            var logger = new iischef.logger.SystemLogger("Application");

            // Prepare a local artifact from path
            string dir = UtilsSystem.GetTempPath("iischeftest1");

            UtilsSystem.DeleteDirectory(dir);

            var chefv1 = UtilsSystem.GetResourceFileAsPath("samples/chef.yml");
            var sampleArtifact = UtilsSystem.GetResourceFileAsPath("samples/sample-php-artifact.zip");

            ZipFile.ExtractToDirectory(sampleArtifact, dir);
            File.Copy(chefv1, UtilsSystem.EnsureDirectoryExists(UtilsSystem.CombinePaths(dir, "chef", "chef.yml")), true);

            // Let's grab the
            var tempSettings = UtilsSystem.EnsureDirectoryExists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\iischef\\server-settings.json");
            File.WriteAllText(tempSettings, UtilsSystem.GetResourceFileAsString("samples/server-settings.json"));

            var app = new Application(logger);
            app.Initialize(tempSettings, "testenvironment");
            app.UseParentLogger();

            string installedAppConfiguration = null;

            installedAppConfiguration = UtilsSystem.GetResourceFileAsString("samples/sample-app-local.yml");
            installedAppConfiguration = installedAppConfiguration.Replace("%PATH%", dir);
            installedAppConfiguration = installedAppConfiguration.Replace("%MOUNTSTRATEGY%", mountstrategy);

            Deployment deployment;

            deployment = app.DeploySingleAppFromTextSettings(installedAppConfiguration);

            // Make sure that the offline site has AUTOSTART false!
            using (var sm = new ServerManager())
            {
                var site = UtilsIis.FindSiteWithName(sm, "off_" + deployment.getShortId(), logger).Single();
                Assert.False(site.ServerAutoStart);
            }

            Action<string> testContains = (string uri) =>
            {
                AssertUriContains(uri, new List<string>() {
                        "Hello world!"
                }, null);
            };

            Action<string> testContains2 = (string uri) =>
            {
                AssertUriContains(uri, new List<string>() {
                    "Maintenance"
                },
                    null,
                    HttpStatusCode.ServiceUnavailable);
            };

            try
            {
                testContains("http://chef.testing.framework");
                testContains("http://chef.testing.framework.private");
                testContains("http://local.chefcdn.com/mytestapplicationdirectory");
                testContains("http://local.chefcdn.com/mytestapplicationdirectory/");

                app.StopAppById(deployment.installedApplicationSettings.GetId());

                using (var sm = new ServerManager())
                {
                    var site = UtilsIis.FindSiteWithName(sm, deployment.getShortId(), logger).Single();
                    Assert.False(site.ServerAutoStart);
                    site = UtilsIis.FindSiteWithName(sm, "off_" + deployment.getShortId(), logger).Single();
                    Assert.True(site.ServerAutoStart);
                }

                testContains2("http://chef.testing.framework");
                testContains2("http://chef.testing.framework.private");
                testContains2("http://local.chefcdn.com/mytestapplicationdirectory");
                testContains2("http://local.chefcdn.com/mytestapplicationdirectory/");

                app.StartAppById(deployment.installedApplicationSettings.GetId());

                using (var sm = new ServerManager())
                {
                    var site = UtilsIis.FindSiteWithName(sm, deployment.getShortId(), logger).Single();
                    Assert.True(site.ServerAutoStart);
                    site = UtilsIis.FindSiteWithName(sm, "off_" + deployment.getShortId(), logger).Single();
                    Assert.False(site.ServerAutoStart);
                }

                testContains("http://chef.testing.framework");
                testContains("http://chef.testing.framework.private");
                testContains("http://local.chefcdn.com/mytestapplicationdirectory");
                testContains("http://local.chefcdn.com/mytestapplicationdirectory/");

                app.StopAppById(deployment.installedApplicationSettings.GetId());

                using (var sm = new ServerManager())
                {
                    var site = UtilsIis.FindSiteWithName(sm, deployment.getShortId(), logger).Single();
                    Assert.False(site.ServerAutoStart);
                    site = UtilsIis.FindSiteWithName(sm, "off_" + deployment.getShortId(), logger).Single();
                    Assert.True(site.ServerAutoStart);
                }

                testContains2("http://chef.testing.framework");
                testContains2("http://chef.testing.framework.private");
                testContains2("http://local.chefcdn.com/mytestapplicationdirectory");
                testContains2("http://local.chefcdn.com/mytestapplicationdirectory/");
            }
            finally
            {
                app.RemoveAppById(deployment.installedApplicationSettings.GetId());

                // If the folder was moved, nothing to delete...
                if (mountstrategy != "move")
                {
                    UtilsSystem.DeleteDirectory(dir, true);
                }
            }
        }

        protected void _TestDeployPhpRuntimeV1(string mountstrategy)
        {
            DoIisReset();

            var logger = new iischef.logger.SystemLogger("Application");

            // Prepare a local artifact from path
            string dir = UtilsSystem.GetTempPath("iischeftest1");

            UtilsSystem.DeleteDirectory(dir);

            var chefv1 = UtilsSystem.GetResourceFileAsPath("samples/chef.yml");
            var sampleArtifact = UtilsSystem.GetResourceFileAsPath("samples/sample-php-artifact.zip");


            ZipFile.ExtractToDirectory(sampleArtifact, dir);
            File.Copy(chefv1, UtilsSystem.EnsureDirectoryExists(UtilsSystem.CombinePaths(dir, "chef", "chef.yml")), true);

            // Let's grab the
            var tempSettings = UtilsSystem.EnsureDirectoryExists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\iischef\\server-settings.json");
            File.WriteAllText(tempSettings, UtilsSystem.GetResourceFileAsString("samples/server-settings.json"));

            var app = new Application(logger);
            app.Initialize(tempSettings, "testenvironment");
            app.UseParentLogger();

            string installedAppConfiguration = null;

            installedAppConfiguration = UtilsSystem.GetResourceFileAsString("samples/sample-app-local.yml");
            installedAppConfiguration = installedAppConfiguration.Replace("%PATH%", dir);
            installedAppConfiguration = installedAppConfiguration.Replace("%MOUNTSTRATEGY%", mountstrategy);

            Deployment deployment;

            deployment = app.DeploySingleAppFromTextSettings(installedAppConfiguration);

            Action<string> testContains = (string uri) =>
            {
                AssertUriContains(uri, new List<string>() {
                        "Hello world!",
                        "deployment.custom.setting",
                        "mycustomserverleveloverride",
                        "my_custom_setting",
                        "services.sqlsrv.username",
                        "services.sqlsrv.password",
                        "services.sqlsrv.database",
                        "services.sqlsrv.host",
                        "\"deployment.artifact.branch\": \"mainbranch\"",
                        "\"deployment.artifact.commit_sha\": \"5a1efde8b452e7a385592aa98a93066aa20282e5\"",
                        "services.couchbase-cache.uri",
                        "services.couchbase-cache.bucket-name",
                        "services.couchbase-cache.bucket-password"
                    }, null);
            };

            Action<string> testContains2 = (string uri) =>
            {
                AssertUriContains(uri, new List<string>() {
                        "e:/whatisthis/whatitis",
                        "e:/whatisthis/whatitis/",
                    }, null);
            };

            try
            {
                testContains2("http://chef.testing.framework/config.php");

                testContains("http://chef.testing.framework");
                testContains("http://chef.testing.framework.private");
                testContains("https://chef.testing.framework.nexus.sabentis.com");

                testContains("http://local.chefcdn.com/mytestapplicationdirectory");
                testContains("http://local.chefcdn.com/mytestapplicationdirectory/");

                var settings = app.GetGlobalSettings();
                AssertStringEquals("myusername", settings.accounts.First().username);

                // If you deploy an artifact with "move"... then the original artifact is lost
                // and you cannot redeploy. TODO: Store copies of these?
                if (mountstrategy != "move")
                {
                    deployment =
                        app.RedeployInstalledApplication(false, deployment.installedApplicationSettings.GetId(), true)
                            .Single();

                    testContains("http://chef.testing.framework");
                    testContains("http://chef.testing.framework.private");
                    testContains("https://chef.testing.framework.nexus.sabentis.com");

                    testContains("http://local.chefcdn.com/mytestapplicationdirectory");
                    testContains("http://local.chefcdn.com/mytestapplicationdirectory/");

                    var deployer = app.GetDeployer(deployment.installedApplicationSettings);
                    deployer.RunCron();
                    deployer.CleanupApp();
                }
            }
            finally
            {
                app.RemoveAppById(deployment.installedApplicationSettings.GetId());

                // If the folder was moved, nothing to delete...
                if (mountstrategy != "move")
                {
                    UtilsSystem.DeleteDirectory(dir, true);
                }
            }
        }

        [Fact]
        [Trait("Category", "Core")]
        public void TestDeployPhpRuntimeV2()
        {
            DoIisReset();

            var logger = new iischef.logger.MemoryLogger();

            // Prepare a local artifact from path
            string dir = UtilsSystem.GetTempPath("iischeftest2");

            UtilsSystem.DeleteDirectory(dir);

            var chefv2 = UtilsSystem.GetResourceFileAsPath("samples/chefv2.yml");
            var chefv2Local = UtilsSystem.GetResourceFileAsPath("samples/chef__local.yml");
            var chefv2Local2 = UtilsSystem.GetResourceFileAsPath("samples/chef__local2.yml");
            var sampleArtifact = UtilsSystem.GetResourceFileAsPath("samples/sample-php-artifact.zip");

            ZipFile.ExtractToDirectory(sampleArtifact, dir);
            File.Copy(chefv2, UtilsSystem.EnsureDirectoryExists(UtilsSystem.CombinePaths(dir, "chef", "chef.yml")), true);
            File.Copy(chefv2Local, UtilsSystem.EnsureDirectoryExists(UtilsSystem.CombinePaths(dir, "chef", "chef__local.yml")));
            File.Copy(chefv2Local2, UtilsSystem.EnsureDirectoryExists(UtilsSystem.CombinePaths(dir, "chef", "chef__local2.yml")));

            // Let's grab the
            var tempSettings = UtilsSystem.EnsureDirectoryExists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\iischef\\server-settings.json");
            File.WriteAllText(tempSettings, UtilsSystem.GetResourceFileAsString("samples/server-settings.json"));

            var app = new Application(logger);
            app.Initialize(tempSettings, "testenvironment");
            app.UseParentLogger();

            string installedAppConfiguration = UtilsSystem.GetResourceFileAsString("samples/sample-app-local.yml");
            installedAppConfiguration = installedAppConfiguration.Replace("%PATH%", dir);
            installedAppConfiguration = installedAppConfiguration.Replace("%MOUNTSTRATEGY%", "link");

            Deployment deployment;

            deployment = app.DeploySingleAppFromTextSettings(installedAppConfiguration);


            Action<string> testContains = (string uri) =>
            {

                AssertUriContains(uri, new List<string>() {
                       "Hello world!",
                        "deployment.custom.setting",
                        "mycustomserverleveloverride",
                        "my_custom_setting",
                        "my_overriden_local_setting",
                        "services.sqlsrv.username",
                        "services.sqlsrv.password",
                        "services.sqlsrv.database",
                        "services.sqlsrv.host",
                        "services.couchbase-cache.uri",
                        "services.couchbase-cache.bucket-name",
                        "services.couchbase-cache.bucket-password",
                        "\"deployment.artifact.branch\": \"mainbranch\"",
                        "\"deployment.artifact.commit_sha\": \"5a1efde8b452e7a385592aa98a93066aa20282e5\""
                    }, new List<string>() {

                        "my_overriden_local_setting2"
                    });
            };

            try
            {
                testContains("https://chef.testing.framework.nexus.sabentis.com");

                var settings = app.GetGlobalSettings();
                AssertStringEquals("myusername", settings.accounts.First().username);

                app.RedeployInstalledApplication(false, deployment.installedApplicationSettings.GetId(), true);

                testContains("http://chef.testing.framework");
                testContains("http://chef.testing.framework.private");
                testContains("http://local.chefcdn.com/mytestapplicationdirectory");
                testContains("http://local.chefcdn.com/mytestapplicationdirectory/");

                var logcontents = logger.GetLog();
                Assert.True(logcontents.Contains("Tried to override mount path (services.contents.mount.files.path) with a non-existent directory"));
            }
            finally
            {
                app.RemoveAppById(deployment.installedApplicationSettings.GetId());
                UtilsSystem.DeleteDirectory(dir, true);
            }

            ZipFile.ExtractToDirectory(sampleArtifact, dir);
            File.Copy(chefv2, UtilsSystem.EnsureDirectoryExists(UtilsSystem.CombinePaths(dir, "chef", "chef.yml")), true);

            app = new Application(logger);
            app.Initialize(tempSettings, "testenvironment");
            app.UseParentLogger();

            deployment = app.DeploySingleAppFromTextSettings(installedAppConfiguration);
            app.RemoveAppById(deployment.installedApplicationSettings.GetId());

            UtilsSystem.DeleteDirectory(dir);
            File.Delete(tempSettings);
        }

        [Fact]
        [Trait("Category", "Core")]
        public void TestIpRestrictions()
        {
            DoIisReset();

            var logger = new iischef.logger.MemoryLogger();

            // Prepare a local artifact from path
            string dir = UtilsSystem.GetTempPath("iischeftest2");

            UtilsSystem.DeleteDirectory(dir);

            var chefv2 = UtilsSystem.GetResourceFileAsPath("samples/chefv2.yml");
            var chefv2Local = UtilsSystem.GetResourceFileAsPath("samples/chef__local.yml");
            var chefv2Local2 = UtilsSystem.GetResourceFileAsPath("samples/chef__local2.yml");
            var sampleArtifact = UtilsSystem.GetResourceFileAsPath("samples/sample-php-artifact.zip");

            ZipFile.ExtractToDirectory(sampleArtifact, dir);
            File.Copy(chefv2, UtilsSystem.EnsureDirectoryExists(UtilsSystem.CombinePaths(dir, "chef", "chef.yml")), true);
            File.Copy(chefv2Local, UtilsSystem.EnsureDirectoryExists(UtilsSystem.CombinePaths(dir, "chef", "chef__local.yml")));
            File.Copy(chefv2Local2, UtilsSystem.EnsureDirectoryExists(UtilsSystem.CombinePaths(dir, "chef", "chef__local2.yml")));

            // Let's grab the
            var tempSettings = UtilsSystem.EnsureDirectoryExists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\iischef\\server-settings.json");
            File.WriteAllText(tempSettings, UtilsSystem.GetResourceFileAsString("samples/server-settings.json"));

            var app = new Application(logger);
            app.Initialize(tempSettings, "testenvironment");
            app.UseParentLogger();

            string installedAppConfiguration = UtilsSystem.GetResourceFileAsString("samples/sample-app-local.yml");
            installedAppConfiguration = installedAppConfiguration.Replace("%PATH%", dir);
            installedAppConfiguration = installedAppConfiguration.Replace("%MOUNTSTRATEGY%", "link");

            Deployment deployment;

            deployment = app.DeploySingleAppFromTextSettings(installedAppConfiguration);

            try
            {
                var headers = new Dictionary<string, string>();

                var settings = app.GetGlobalSettings();
                AssertStringEquals("myusername", settings.accounts.First().username);

                headers["X-Forwareded-For"] = "148.142.5.10";

                UtilsSystem.DownloadUriAsText("http://chef.testing.framework", true, HttpStatusCode.OK);
                UtilsSystem.DownloadUriAsText("http://chef.testing.framework", true, HttpStatusCode.OK);
                UtilsSystem.DownloadUriAsText("http://chef.testing.framework", true, HttpStatusCode.OK);
                UtilsSystem.DownloadUriAsText("http://chef.testing.framework", true, HttpStatusCode.OK);
                UtilsSystem.DownloadUriAsText("http://chef.testing.framework", true, HttpStatusCode.OK);
                UtilsSystem.DownloadUriAsText("http://chef.testing.framework", true, HttpStatusCode.OK);
                UtilsSystem.DownloadUriAsText("http://chef.testing.framework", true, HttpStatusCode.OK);
                UtilsSystem.DownloadUriAsText("http://chef.testing.framework", true, HttpStatusCode.OK);
                UtilsSystem.DownloadUriAsText("http://chef.testing.framework", true, HttpStatusCode.OK);
                UtilsSystem.DownloadUriAsText("http://chef.testing.framework", true, HttpStatusCode.OK);
                UtilsSystem.DownloadUriAsText("http://chef.testing.framework", true, HttpStatusCode.Forbidden);

                headers["X-Forwareded-For"] = "148.142.12.10";

                UtilsSystem.DownloadUriAsText("http://chef.testing.framework", true, HttpStatusCode.OK, headers);
                UtilsSystem.DownloadUriAsText("http://chef.testing.framework", true, HttpStatusCode.OK, headers);
                UtilsSystem.DownloadUriAsText("http://chef.testing.framework", true, HttpStatusCode.OK, headers);
                UtilsSystem.DownloadUriAsText("http://chef.testing.framework", true, HttpStatusCode.OK, headers);
                UtilsSystem.DownloadUriAsText("http://chef.testing.framework", true, HttpStatusCode.OK, headers);
                UtilsSystem.DownloadUriAsText("http://chef.testing.framework", true, HttpStatusCode.OK, headers);
                UtilsSystem.DownloadUriAsText("http://chef.testing.framework", true, HttpStatusCode.OK, headers);
                UtilsSystem.DownloadUriAsText("http://chef.testing.framework", true, HttpStatusCode.OK, headers);
                UtilsSystem.DownloadUriAsText("http://chef.testing.framework", true, HttpStatusCode.OK, headers);
                UtilsSystem.DownloadUriAsText("http://chef.testing.framework", true, HttpStatusCode.OK, headers);
                UtilsSystem.DownloadUriAsText("http://chef.testing.framework", true, HttpStatusCode.OK, headers);
                UtilsSystem.DownloadUriAsText("http://chef.testing.framework", true, HttpStatusCode.OK, headers);

                headers["X-Forwareded-For"] = "127.0.0.1, 148.142.12.10:8080";

                UtilsSystem.DownloadUriAsText("http://chef.testing.framework", true, HttpStatusCode.OK, headers);
                UtilsSystem.DownloadUriAsText("http://chef.testing.framework", true, HttpStatusCode.OK, headers);
                UtilsSystem.DownloadUriAsText("http://chef.testing.framework", true, HttpStatusCode.OK, headers);
                UtilsSystem.DownloadUriAsText("http://chef.testing.framework", true, HttpStatusCode.OK, headers);
                UtilsSystem.DownloadUriAsText("http://chef.testing.framework", true, HttpStatusCode.OK, headers);
                UtilsSystem.DownloadUriAsText("http://chef.testing.framework", true, HttpStatusCode.OK, headers);
                UtilsSystem.DownloadUriAsText("http://chef.testing.framework", true, HttpStatusCode.OK, headers);
                UtilsSystem.DownloadUriAsText("http://chef.testing.framework", true, HttpStatusCode.OK, headers);
                UtilsSystem.DownloadUriAsText("http://chef.testing.framework", true, HttpStatusCode.OK, headers);
                UtilsSystem.DownloadUriAsText("http://chef.testing.framework", true, HttpStatusCode.OK, headers);
                UtilsSystem.DownloadUriAsText("http://chef.testing.framework", true, HttpStatusCode.OK, headers);
                UtilsSystem.DownloadUriAsText("http://chef.testing.framework", true, HttpStatusCode.OK, headers);

                headers["X-Forwareded-For"] = "127.0.0.1, 145.85.25.4:443";

                UtilsSystem.DownloadUriAsText("http://chef.testing.framework", true, HttpStatusCode.OK);
                UtilsSystem.DownloadUriAsText("http://chef.testing.framework", true, HttpStatusCode.OK);
                UtilsSystem.DownloadUriAsText("http://chef.testing.framework", true, HttpStatusCode.OK);
                UtilsSystem.DownloadUriAsText("http://chef.testing.framework", true, HttpStatusCode.OK);
                UtilsSystem.DownloadUriAsText("http://chef.testing.framework", true, HttpStatusCode.OK);
                UtilsSystem.DownloadUriAsText("http://chef.testing.framework", true, HttpStatusCode.OK);
                UtilsSystem.DownloadUriAsText("http://chef.testing.framework", true, HttpStatusCode.OK);
                UtilsSystem.DownloadUriAsText("http://chef.testing.framework", true, HttpStatusCode.OK);
                UtilsSystem.DownloadUriAsText("http://chef.testing.framework", true, HttpStatusCode.OK);
                UtilsSystem.DownloadUriAsText("http://chef.testing.framework", true, HttpStatusCode.OK);
                UtilsSystem.DownloadUriAsText("http://chef.testing.framework", true, HttpStatusCode.Forbidden);

                headers["X-Forwareded-For"] = "148.142.12.15";

                UtilsSystem.DownloadUriAsText("http://chef.testing.framework", true, HttpStatusCode.Forbidden, headers);
            }
            finally
            {
                app.RemoveAppById(deployment.installedApplicationSettings.GetId());
                UtilsSystem.DeleteDirectory(dir, true);
            }

            UtilsSystem.DeleteDirectory(dir);
            File.Delete(tempSettings);
        }

        [Fact]
        [Trait("Category", "Core")]
        public void TestCertificateRenewalLogic()
        {
            DoIisReset();

            var logger = new iischef.logger.MemoryLogger();
            logger.SetVerbose(true);

            // Prepare a local artifact from path
            string dir = UtilsSystem.GetTempPath("iischeftest2");

            UtilsSystem.DeleteDirectory(dir);

            var chefv2 = UtilsSystem.GetResourceFileAsPath("samples/chefv2.yml");
            var chefv2Local = UtilsSystem.GetResourceFileAsPath("samples/chef__local.yml");
            var chefv2Local2 = UtilsSystem.GetResourceFileAsPath("samples/chef__local2.yml");
            var sampleArtifact = UtilsSystem.GetResourceFileAsPath("samples/sample-php-artifact.zip");

            ZipFile.ExtractToDirectory(sampleArtifact, dir);
            File.Copy(chefv2, UtilsSystem.EnsureDirectoryExists(UtilsSystem.CombinePaths(dir, "chef", "chef.yml")), true);
            File.Copy(chefv2Local, UtilsSystem.EnsureDirectoryExists(UtilsSystem.CombinePaths(dir, "chef", "chef__local.yml")));
            File.Copy(chefv2Local2, UtilsSystem.EnsureDirectoryExists(UtilsSystem.CombinePaths(dir, "chef", "chef__local2.yml")));

            // Let's grab the
            var tempSettings = UtilsSystem.EnsureDirectoryExists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\iischef\\server-settings.json");
            File.WriteAllText(tempSettings, UtilsSystem.GetResourceFileAsString("samples/server-settings.json"));

            var app = new Application(logger);
            app.Initialize(tempSettings, "testenvironment");
            app.UseParentLogger();

            string installedAppConfiguration = UtilsSystem.GetResourceFileAsString("samples/sample-app-local.yml");
            installedAppConfiguration = installedAppConfiguration.Replace("%PATH%", dir);
            installedAppConfiguration = installedAppConfiguration.Replace("%MOUNTSTRATEGY%", "link");

            // Get rid of any existing certificate
            UtilsIis.FindCertificateInCentralCertificateStore("chef.testing.framework.nexus.sabentis.com", logger, out var certificatePath);

            if (File.Exists(certificatePath))
            {
                File.Delete(certificatePath);
            }

            DoIisReset();

            var deployment = app.DeploySingleAppFromTextSettings(installedAppConfiguration);

            Action<string> testContains = (string uri) =>
            {

                AssertUriContains(uri, new List<string>() {
                       "Hello world!",
                }, new List<string>() {

                        "my_overriden_local_setting2"
                    });
            };

            try
            {
                // Initiail HTTPS load
                UtilsIis.EnsureCertificateInCentralCertificateStoreIsRebound("chef.testing.framework.nexus.sabentis.com", logger);

                testContains("https://chef.testing.framework.nexus.sabentis.com");
                Assert.Contains("Generating self signed certificate.", logger.GetLog());

                logger.Clear();

                deployment = app.DeploySingleAppFromTextSettings(installedAppConfiguration);

                testContains("https://chef.testing.framework.nexus.sabentis.com");

                Assert.DoesNotContain("Generating self signed certificate.", logger.GetLog());
                Assert.Contains("skipping SSL provisioning", logger.GetLog());

                // There is a bug in IIS where if we update the certificate in the central store,
                // but do not reconfigure IIS's HTTPS bindings, it fails to bind to the certificate.
                UtilsIis.FindCertificateInCentralCertificateStore("chef.testing.framework.nexus.sabentis.com", logger, out certificatePath);
                File.Delete(certificatePath);

                // A cron loop will renew the certificate
                logger.Clear();
                UtilsIis.EnsureCertificateInCentralCertificateStoreIsRebound("chef.testing.framework.nexus.sabentis.com", logger);

                // Now we query the site and there is an ERROR, as there is no certificate...
                var ex = Assert.ThrowsAny<Exception>(() =>
                {
                    testContains("https://chef.testing.framework.nexus.sabentis.com");
                });

                Assert.Equal("-2146233079", ex.HResult.ToString());

                // This will issue a new certificate
                logger.Clear();
                app.RunCron(deployment.installedApplicationSettings.GetId());
                Assert.Contains("Generating self signed certificate.", logger.GetLog());
                testContains("https://chef.testing.framework.nexus.sabentis.com");

                // This will not issue a certificate
                logger.Clear();
                app.DeploySsl(deployment.installedApplicationSettings.GetId());
                Assert.DoesNotContain("Generating self signed certificate.", logger.GetLog());
                testContains("https://chef.testing.framework.nexus.sabentis.com");

                // This will issue a new certificate
                logger.Clear();
                app.DeploySsl(deployment.installedApplicationSettings.GetId(), true);
                Assert.DoesNotContain("A self-signed certificate is provisioned on first installation of an application", logger.GetLog());
                Assert.Contains("Generating self signed certificate.", logger.GetLog());
                testContains("https://chef.testing.framework.nexus.sabentis.com");
            }
            finally
            {
                app.RemoveAppById(deployment.installedApplicationSettings.GetId());
                UtilsSystem.DeleteDirectory(dir, true);
            }
        }

        [Fact]
        [Trait("Category", "Core")]
        public void TestDeployLocalZip()
        {
            DoIisReset();

            var logger = new iischef.logger.MemoryLogger();

            var sampleArtifact = UtilsSystem.GetResourceFileAsPath("samples/sample-php-artifact.zip");

            // Let's grab the
            var tempSettings = UtilsSystem.EnsureDirectoryExists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\iischef\\server-settings.json");
            File.WriteAllText(tempSettings, UtilsSystem.GetResourceFileAsString("samples/server-settings.json"));

            var app = new Application(logger);
            app.Initialize(tempSettings, "testenvironment");
            app.UseParentLogger();

            string installedAppConfiguration = UtilsSystem.GetResourceFileAsString("samples/sample-app-localzip.yml");
            installedAppConfiguration = installedAppConfiguration.Replace("%PATH%", sampleArtifact);

            Deployment deployment;

            deployment = app.DeploySingleAppFromTextSettings(installedAppConfiguration);

            Action<string> testContains = (string uri) =>
            {

                AssertUriContains(uri, new List<string>() {
                       "Hello world!",
                        "services.sqlsrv.username",
                        "services.sqlsrv.password",
                        "services.sqlsrv.database",
                        "services.sqlsrv.host",
                        "\"cdn.preferred_prefix\": \"https:\\/\\/external-url2\\/cdn_chef-appveyor-sample-localzip\\/\"",
                }, new List<string>() {
                        "my_overriden_local_setting2"
                    });
            };

            try
            {
                testContains("http://chef.testing.framework");
                testContains("http://mainbranch.testing.framework");
                testContains("http://chef.testing.framework.private");
                testContains("http://local.chefcdn.com/mytestapplicationdirectory");
                testContains("http://local.chefcdn.com/mytestapplicationdirectory/");

                // Test the canonical CDN
                testContains("http://local.chefcdn.com/cdn_chef-appveyor-sample-localzip/");

                // Test cache buster in the CDN
                testContains("http://local.chefcdn.com/cachebuster_1265489/cdn_chef-appveyor-sample-localzip/");

                Assert.ThrowsAny<Exception>(() =>
                {
                    testContains("http://local.chefcdn.com/cachebuster_sdg9/cdn_chef-appveyor-sample-localzip/");
                });

                var settings = app.GetGlobalSettings();
                AssertStringEquals("myusername", settings.accounts.First().username);

                app.RedeployInstalledApplication(false, deployment.installedApplicationSettings.GetId(), true);

                testContains("http://chef.testing.framework");
                testContains("http://chef.testing.framework.private");
                testContains("http://local.chefcdn.com/mytestapplicationdirectory");
                testContains("http://local.chefcdn.com/mytestapplicationdirectory/");

                // Test the canonical CDN
                testContains("http://local.chefcdn.com/cdn_chef-appveyor-sample-localzip/");
            }
            finally
            {
                app.RemoveAppById(deployment.installedApplicationSettings.GetId());
            }

            File.Delete(tempSettings);
        }

        [Fact]
        [Trait("Category", "Core")]
        public void TestDeployPhpRuntimeV3()
        {
            DoIisReset();

            var logger = new iischef.logger.MemoryLogger();

            // Prepare a local artifact from path
            string dir = UtilsSystem.GetTempPath("iischeftest2");

            UtilsSystem.DeleteDirectory(dir);

            var chefv2 = UtilsSystem.GetResourceFileAsPath("samples/chefv2.yml");
            var chefv2Local = UtilsSystem.GetResourceFileAsPath("samples/chef__local.yml");
            var chefv2Local2 = UtilsSystem.GetResourceFileAsPath("samples/chef__local2.yml");
            var sampleArtifact = UtilsSystem.GetResourceFileAsPath("samples/sample-php-artifact.zip");


            ZipFile.ExtractToDirectory(sampleArtifact, dir);
            File.Copy(chefv2, UtilsSystem.EnsureDirectoryExists(UtilsSystem.CombinePaths(dir, "chef", "chef.yml")), true);
            File.Copy(chefv2Local, UtilsSystem.EnsureDirectoryExists(UtilsSystem.CombinePaths(dir, "chef", "chef__local.yml")));
            File.Copy(chefv2Local2, UtilsSystem.EnsureDirectoryExists(UtilsSystem.CombinePaths(dir, "chef", "chef__local2.yml")));

            // Let's grab the
            var tempSettings = UtilsSystem.EnsureDirectoryExists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\iischef\\server-settings.json");
            File.WriteAllText(tempSettings, UtilsSystem.GetResourceFileAsString("samples/server-settings.json"));

            var app = new Application(logger);
            app.Initialize(tempSettings, "testenvironment");
            app.UseParentLogger();

            string installedAppConfiguration = UtilsSystem.GetResourceFileAsString("samples/sample-app-local.yml");
            installedAppConfiguration = installedAppConfiguration.Replace("%PATH%", dir);
            installedAppConfiguration = installedAppConfiguration.Replace("%MOUNTSTRATEGY%", "link");

            Deployment deployment;

            deployment = app.DeploySingleAppFromTextSettings(installedAppConfiguration, false, "latest");
            deployment = app.DeploySingleAppFromTextSettings(installedAppConfiguration, false, "1.0");

            try
            {
                deployment = app.DeploySingleAppFromTextSettings(installedAppConfiguration, true);
            }
            catch (Exception e)
            {
                Assert.True(e.Message.Contains("Deployment was skipped because previous deployment was a version-specific deployment"));
            }

            app.RemoveAppById(deployment.installedApplicationSettings.GetId());

            UtilsSystem.DeleteDirectory(dir);
            File.Delete(tempSettings);

        }

        [Fact]
        [Trait("Category", "Core")]
        public void TestAlreadyInstalledCertificate()
        {
            DoIisReset();

            // Provision a certificate
            var cert = UtilsCertificate.CreateSelfSignedCertificate(
                "chef.testing.framework.sabentis.com",
                "test-certificate-chef",
                15);

            var logger = new iischef.logger.MemoryLogger();

            // Prepare a local artifact from path
            string dir = UtilsSystem.GetTempPath("iischeftest2");

            UtilsSystem.DeleteDirectory(dir);

            var chefv2 = UtilsSystem.GetResourceFileAsPath("samples/chefv2_certificate.yml");
            var sampleArtifact = UtilsSystem.GetResourceFileAsPath("samples/sample-php-artifact.zip");


            ZipFile.ExtractToDirectory(sampleArtifact, dir);
            File.Copy(chefv2, UtilsSystem.EnsureDirectoryExists(UtilsSystem.CombinePaths(dir, "chef", "chef.yml")), true);

            var tempSettings = UtilsSystem.EnsureDirectoryExists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\iischef\\server-settings.json");
            File.WriteAllText(tempSettings, UtilsSystem.GetResourceFileAsString("samples/server-settings.json"));

            var app = new Application(logger);
            app.Initialize(tempSettings, "testenvironment");
            app.UseParentLogger();

            string installedAppConfiguration = UtilsSystem.GetResourceFileAsString("samples/sample-app-local.yml");
            installedAppConfiguration = installedAppConfiguration.Replace("%PATH%", dir);
            installedAppConfiguration = installedAppConfiguration.Replace("%MOUNTSTRATEGY%", "link");

            var deployment = app.DeploySingleAppFromTextSettings(installedAppConfiguration, false, "latest");

            AssertUriContains(
                "https://chef.testing.framework.sabentis.com",
                new List<string>() { "https:\\/\\/chef.testing.framework.sabentis.com:443" },
                new List<string>() { });

            app.RemoveAppById(deployment.installedApplicationSettings.GetId());

            // Delete the certificate
            var store = new X509Store(StoreName.My, StoreLocation.LocalMachine);
            store.Open(OpenFlags.OpenExistingOnly | OpenFlags.ReadWrite);
            store.Remove(cert);
            store.Close();

            UtilsSystem.DeleteDirectory(dir);
            File.Delete(tempSettings);
        }

        /// <summary>
        /// Test the service locally and manually.
        /// </summary>
        [Fact]
        [Trait("Category", "Core")]
        public void TestServiceLocalManual()
        {
            return;
            var service = new ApplicationService();
            service.Start();

            while (true)
            {
                System.Threading.Thread.Sleep(600);
            }
        }

        [Fact]
        [Trait("Category", "Core")]
        public void TestExceptionWhenCdNbindingWrong()
        {
            DoIisReset();

            var logger = new iischef.logger.SystemLogger("Application");

            // Prepare a local artifact from path
            string dir = UtilsSystem.GetTempPath("iischeftest1");

            UtilsSystem.DeleteDirectory(dir);

            var chefv1 = UtilsSystem.GetResourceFileAsPath("samples/chefCDNBindings.yml");
            var sampleArtifact = UtilsSystem.GetResourceFileAsPath("samples/sample-php-artifact.zip");

            ZipFile.ExtractToDirectory(sampleArtifact, dir);
            File.Copy(chefv1, UtilsSystem.EnsureDirectoryExists(UtilsSystem.CombinePaths(dir, "chef", "chefCDNBindings.yml")));

            // Let's grab the
            var tempSettings = UtilsSystem.EnsureDirectoryExists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\iischef\\server-settings.json");
            File.WriteAllText(tempSettings, UtilsSystem.GetResourceFileAsString("samples/server-settings.json"));

            var app = new Application(logger);
            app.Initialize(tempSettings, "testenvironment");
            app.UseParentLogger();

            string installedAppConfiguration = null;

            installedAppConfiguration = UtilsSystem.GetResourceFileAsString("samples/sample-app-local.yml");
            installedAppConfiguration = installedAppConfiguration.Replace("%PATH%", dir);
            installedAppConfiguration = installedAppConfiguration.Replace("%MOUNTSTRATEGY%", "copy");

            Deployment deployment = null;
            bool hasException = false;
            try
            {
                deployment = app.DeploySingleAppFromTextSettings(installedAppConfiguration);
            }
            catch (Exception e)
            {
                hasException = true;
                AssertStringContains("cdn_mount", e.Message);
            }
            finally
            {
                if (!hasException)
                {
                    app.RemoveAppById(deployment.installedApplicationSettings.GetId());
                }
                // If the folder was moved, nothing to delete...
                if ("copy" != "move")
                {
                    UtilsSystem.DeleteDirectory(dir, true);
                }
                // Fail test if exception was not thrown
                AssertStringContains(hasException.ToString(), true.ToString());
            }
        }

        [Fact]
        [Trait("Category", "Core")]
        public void TestSync()
        {
            DoIisReset();

            var logger = new iischef.logger.SystemLogger("Application");

            // Prepare a local artifact from path
            string dir = UtilsSystem.GetTempPath("iischeftest1");

            UtilsSystem.DeleteDirectory(dir);

            var chefv1 = UtilsSystem.GetResourceFileAsPath("samples/chef.yml");
            var sampleArtifact = UtilsSystem.GetResourceFileAsPath("samples/sample-php-artifact.zip");


            ZipFile.ExtractToDirectory(sampleArtifact, dir);
            File.Copy(chefv1, UtilsSystem.EnsureDirectoryExists(UtilsSystem.CombinePaths(dir, "chef", "chef.yml")), true);

            // Let's grab the
            var tempSettings = UtilsSystem.EnsureDirectoryExists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\iischef\\server-settings.json");
            File.WriteAllText(tempSettings, UtilsSystem.GetResourceFileAsString("samples/server-settings.json"));

            var app = new Application(logger);
            app.Initialize(tempSettings, "testenvironment");
            app.UseParentLogger();

            string installedAppConfiguration = null;

            installedAppConfiguration = UtilsSystem.GetResourceFileAsString("samples/sample-app-local_sync_1.yml");
            installedAppConfiguration = installedAppConfiguration.Replace("%PATH%", dir);
            installedAppConfiguration = installedAppConfiguration.Replace("%MOUNTSTRATEGY%", "copy");

            Deployment deployment;

            deployment = app.DeploySingleAppFromTextSettings(installedAppConfiguration);

            foreach (var pathSync in System.IO.Directory.EnumerateDirectories(
                deployment.GetRuntimeSettingsToDeploy()["services." + "contents" + ".mount.files.path"],
                "*", SearchOption.AllDirectories))
            {
                File.Create(UtilsSystem.CombinePaths(pathSync, "sync_test.txt")).Close();
            }
            File.Create(UtilsSystem.CombinePaths(deployment.GetRuntimeSettingsToDeploy()["services." + "contents" + ".mount.files.path"], "sync_test.txt")).Close();

            foreach (var server in app.GetGlobalSettings().sqlServers)
            {
                SqlConnection conn = new SqlConnection(server.connectionString);
                conn.Open();
                conn.ChangeDatabase(deployment.GetRuntimeSettingsToDeploy()["services." + "sqlsrv" + ".database"]);
                SqlCommand cm00 = new SqlCommand("IF OBJECT_ID('dbo.a', 'U') IS NOT NULL DROP TABLE dbo.a;", conn);
                cm00.ExecuteNonQuery();
                SqlCommand cm0 = new SqlCommand("CREATE TABLE a (ID char(5) NOT NULL,PRIMARY KEY(ID));", conn);
                cm0.ExecuteNonQuery();
                SqlCommand cm1 = new SqlCommand("insert into a (id) VALUES ('hello');", conn);
                cm1.ExecuteNonQuery();
                conn.Close();
            }

            // Deploy son application
            string installedAppConfigurationSon = null;
            installedAppConfigurationSon = UtilsSystem.GetResourceFileAsString("samples/sample-app-local_sync_2.yml");
            installedAppConfigurationSon = installedAppConfigurationSon.Replace("%PATH%", dir);
            installedAppConfigurationSon = installedAppConfigurationSon.Replace("%MOUNTSTRATEGY%", "copy");

            var chefsync = UtilsSystem.GetResourceFileAsPath("samples/cheff_sync.yml");
            File.Delete(UtilsSystem.CombinePaths(dir, "chef", "chef.yml"));
            File.Copy(chefsync, UtilsSystem.EnsureDirectoryExists(UtilsSystem.CombinePaths(dir, "chef", "chef.yml")));

            Deployment deploymentSon;
            deploymentSon = app.DeploySingleAppFromTextSettings(installedAppConfigurationSon);
            //
            try
            {
                foreach (var pathSync in System.IO.Directory.EnumerateDirectories(
                deploymentSon.GetRuntimeSettingsToDeploy()["services." + "contents" + ".mount.files.path"],
                "*", SearchOption.AllDirectories))
                {
                    Assert.True(System.IO.Directory.GetFiles(pathSync).ToList()
                        .Exists(s => s.Contains("sync_test.txt")), "There is NO sync file in " + pathSync);
                }

                Assert.True(System.IO.Directory.GetFiles(deploymentSon.GetRuntimeSettingsToDeploy()["services." + "contents" + ".mount.files.path"])
                    .ToList().Exists(s => s.Contains("sync_test.txt")), "There is NO sync file in Root");

                foreach (var server in app.GetGlobalSettings().sqlServers)
                {
                    SqlConnection conn = new SqlConnection(server.connectionString);
                    conn.Open();
                    conn.ChangeDatabase(deploymentSon.GetRuntimeSettingsToDeploy()["services." + "sqlsrv" + ".database"]);
                    SqlCommand cm = new SqlCommand("select id from a;", conn);
                    SqlDataReader reader = cm.ExecuteReader();
                    Assert.True(reader.HasRows, "The are NO rows");
                    reader.Read();
                    AssertStringEquals("hello", reader.GetString(reader.GetOrdinal("ID")));
                    conn.Close();
                }
            }
            finally
            {
                app.RemoveAppById(deployment.installedApplicationSettings.GetId());
                app.RemoveAppById(deploymentSon.installedApplicationSettings.GetId());
                UtilsSystem.DeleteDirectory(dir, true);
            }
        }

    }
}
