﻿using iischef.core;
using iischef.core.IIS;
using iischef.core.SystemConfiguration;
using iischef.logger;
using iischef.utils;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.IO;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using Xunit;

namespace healthmonitortests
{
    public class FunctionalTests : IClassFixture<ChefTestFixture>
    {
        protected ChefTestFixture Fixture;

        public FunctionalTests(ChefTestFixture fixture)
        {
            this.Fixture = fixture;
        }

        [Fact]
        [Trait("Category", "Functional")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public void TestJsonKvConverter()
        {
            var converter = new iischef.core.Configuration.JObjectToKeyValueConverter();

            var original = JObject.FromObject(
                new
                {
                    mailSettings = new
                    {
                        host = "myhost",
                        port = "myport"
                    },
                    myArray = new List<Object>() {
                        "a",
                        "b",
                        "c",
                        new  {
                            pro1 = "prop1",
                            pro2 = "prop2",
                        }
                    }
                }
           );

            var c1 = converter.NestedToKeyValue(original);

            var serialized = Newtonsoft.Json.JsonConvert.SerializeObject(c1);
            Assert.Equal("{\"mailSettings.host\":\"myhost\",\"mailSettings.port\":\"myport\",\"myArray.0\":\"a\",\"myArray.1\":\"b\",\"myArray.2\":\"c\",\"myArray.3.pro1\":\"prop1\",\"myArray.3.pro2\":\"prop2\"}",
                serialized);

            // Now the other way round...
            JObject reverted = converter.keyValueToNested(c1);

            Assert.Equal(
                Newtonsoft.Json.JsonConvert.SerializeObject(original),
                Newtonsoft.Json.JsonConvert.SerializeObject(reverted)
                );
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        [Trait("Category", "Functional")]
        public void TestSimpleStore()
        {
            var simpleStore = new SimpleStore(UtilsSystem.GetTempPath("samplestore"));
            simpleStore.Clear();

            Assert.False(simpleStore.Get<string>("itemA", out _));

            simpleStore.Set("itemA", "testdataA", 50);

            Assert.True(simpleStore.Get<string>("itemA", out var itemA));
            Assert.Equal("testdataA", itemA.Data);

            Assert.False(simpleStore.Get<string>("itemB", out _));

            simpleStore.Set("itemB", "testdataB", 50);

            Assert.True(simpleStore.Get<string>("itemA", out itemA));
            Assert.Equal("testdataA", itemA.Data);

            Assert.True(simpleStore.Get<string>("itemB", out var itemB));
            Assert.Equal("testdataB", itemB.Data);
        }

        /// <summary>
        /// Test the service locally and manually.
        /// </summary>
        [Fact]
        [Trait("Category", "Core")]
        public void TestSettingsReplacer()
        {
            Dictionary<string, string> settings = new Dictionary<string, string>();
            settings["a"] = "this thing has a backslash and needs to be \\ escaped to be used as a json literal";
            settings["b"] = "this thing has a <html> markup that needs ' to be put inside an XML \" placeholder to be & used as a json literal";

            var replacer = new RuntimeSettingsReplacer(settings);

            Assert.Equal("this thing has a backslash and needs to be \\\\ escaped to be used as a json literal", replacer.DoReplace("{@a|filter: jsonescape@}"));
            Assert.Equal("this thing has a backslash and needs to be \\\\ escaped to be used as a json literal", replacer.DoReplace("{@a|filter:jsonescape@}"));

            Assert.Equal("this thing has a backslash and needs to be / escaped to be used as a json literal", replacer.DoReplace("{@a|filter:allforward@}"));

            Assert.Equal("this thing has a &lt;html&gt; markup that needs &apos; to be put inside an XML &quot; placeholder to be &amp; used as a json literal", replacer.DoReplace("{@b|filter:xmlescape@}"));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        [Trait("Category", "Functional")]
        public void TestWindowsRight()
        {
            LoggerInterface logger = new ConsoleLogger();

            UtilsWindowsAccounts.EnsureUserExists("chf_testaccount", "81Dentiaaeh#" + Guid.NewGuid(), "The display name", logger, null);
            UtilsWindowsAccounts.EnsureGroupExists("chef_testgroup", null);
            UtilsWindowsAccounts.EnsureUserInGroup("chf_testaccount", "chef_testgroup", logger, null);
            Assert.Equal(0, UtilsWindowsAccounts.SetRight("chf_testaccount", "SeCreateSymbolicLinkPrivilege", logger));
            Assert.Equal(0, UtilsWindowsAccounts.SetRight("chf_testaccount", "SeBatchLogonRight", logger));
            UtilsWindowsAccounts.DeleteUser("chf_testaccount", null);
        }

        /// <summary>
        /// Only for manual testing to debug the service.
        /// </summary>
        [Fact]
        [Trait("Category", "Functional")]
        public void TestServiceCanStop()
        {
            var service = new ApplicationService();
            service.Start();

            var start = DateTime.Now;

            while (true)
            {
                System.Threading.Thread.Sleep(400);

                if ((DateTime.Now - start).TotalSeconds > 2)
                {
                    service.Stop();
                    break;
                }
            }
        }

        [Fact]
        [Trait("Category", "Functional")]
        public void TestAcmeVault()
        {
            AcmeSharpProvider.TestVault();
        }

        /// <summary>
        /// Converting PEM to PFX requires some 3d party libraries that brake every once in a while...
        /// </summary>
        [Fact]
        [Trait("Category", "Functional")]
        public void TestPfxFromPem()
        {
            BindingRedirectHandler.DoBindingRedirects(AppDomain.CurrentDomain);

            string pfxFilePath = Path.GetTempPath() + "ssl-pfx-test-" + Guid.NewGuid() + ".pfx";

            string certificatePassword = null;
            string crtFilePath = UtilsSystem.GetResourceFileAsPath("certificate_files_example\\a0e8efb7cca4452ed304b1d9614ec89d-crt.pem");
            string keyFilePath = UtilsSystem.GetResourceFileAsPath("certificate_files_example\\a0e8efb7cca4452ed304b1d9614ec89d-key.pem");

            UtilsCertificate.CreatePfXfromPem(crtFilePath, keyFilePath, pfxFilePath, certificatePassword);

            // Make sure that the certificate file is valid and works
            X509Certificate2Collection collection = new X509Certificate2Collection();

            collection.Import(pfxFilePath, certificatePassword, X509KeyStorageFlags.PersistKeySet);
            var originalCert = collection[0];
            Assert.Equal("033679B39C0CDA50C745ABD173FB0DD381A1", originalCert.SerialNumber);

            File.Delete(pfxFilePath);
        }

        [Fact]
        [Trait("Category", "Functional")]
        public void TestNameParser()
        {
            Assert.Equal("sabentis.loc", new FqdnNameParser("myname@sabentis.loc", false).DomainName);
            Assert.Equal("myname", new FqdnNameParser("myname@sabentis.loc", false).UserPrincipalName);
            Assert.Equal(ContextType.Domain, new FqdnNameParser("myname@sabentis.loc", false).ContextType);

            Assert.Equal("SABENTIS", new FqdnNameParser("SABENTIS\\myname", false).DomainName);
            Assert.Equal("myname", new FqdnNameParser("SABENTIS\\myname", false).UserPrincipalName);
            Assert.Equal(ContextType.Domain, new FqdnNameParser("SABENTIS\\myname", false).ContextType);

            Assert.Equal(ContextType.Machine, new FqdnNameParser("myname@localhost", false).ContextType);
            Assert.Equal(ContextType.Machine, new FqdnNameParser("LOCALHOST\\myname", false).ContextType);

            Assert.NotNull(new FqdnNameParser("S-1-5-32-559").Sid);
            Assert.NotNull(new FqdnNameParser("sid:S-1-5-32-559").Sid);
        }

        [Fact]
        [Trait("Category", "Functional")]
        public void TestIpParser()
        {
            Assert.Equal("192.168.5.3", UtilsIis.GetSubnetAndMaskFromCidr("192.168.5.3/20").Item1.ToString());
            Assert.Equal("255.255.240.0", UtilsIis.GetSubnetAndMaskFromCidr("192.168.5.3/20").Item2.ToString());

            Assert.Equal("192.168.5.3", UtilsIis.GetSubnetAndMaskFromCidr("192.168.5.3").Item1.ToString());
            Assert.Null(UtilsIis.GetSubnetAndMaskFromCidr("192.168.5.3").Item2);
        }

        [Fact]
        [Trait("Category", "Functional")]
        public void TestLongPathSupport()
        {
            string uncPath = "\\\\serverxx\\directory";
            string uncPathLong = UtilsSystem.AddLongPathSupport(uncPath);
            Assert.Equal("\\\\?\\UNC\\serverxx\\directory", uncPathLong);

            // Create a very long filename, individual segments can't be over 255 characters
            string fileName = "c:\\";
            for (int x = 0; x < 100; x++)
            {
                fileName += Guid.NewGuid() + "\\";
            }

            Assert.ThrowsAny<Exception>(() =>
            {
                Directory.CreateDirectory(fileName);
            });

            var fileNameWithLongPathSupport = UtilsSystem.EnsureLongPathSupportIfAvailable(fileName);

            Directory.CreateDirectory(fileNameWithLongPathSupport);

            fileNameWithLongPathSupport += "info.txt";

            File.WriteAllText(fileNameWithLongPathSupport, "empty contents");

            File.Delete(fileNameWithLongPathSupport);
        }

        /// <summary>
        /// Test CDN redirection
        /// </summary>
        [Fact]
        [Trait("Category", "Functional")]
        public void TestCdnReplacements()
        {
            var sampleHtml = File.ReadAllText(UtilsSystem.GetResourceFileAsPath("samples/samplecdn.html"));
            
            var replacer = new CdnHtmlRedirectHelper();

            replacer.PrependCdnToUri(sampleHtml, "https://cdnprefix/directory/");
        }

        /// <summary>
        /// Test CDN redirection
        /// </summary>
        [Fact]
        [Trait("Category", "Functional")]
        public void TestGetMaxWebConfigFileSizeInKB()
        {
            UtilsIis.GetMaxWebConfigFileSizeInKb();
        }

        /// <summary>
        /// Test than we can provision self-signed certificates, and that we can remove them
        /// </summary>
        [Fact]
        [Trait("Category", "Functional")]
        public void GenerateSampleApplicationSettings()
        {
            var settings = new EnvironmentSettings();

            settings.couchbaseServers = new List<CouchbaseServer>();
            settings.couchbaseServers.Add(new CouchbaseServer()
            {
                id = "default",
                uri = "couchbase://127.0.0.1",
                bucketName = "couch",
                bucketPassword = "couch"
            });

            settings.primaryCouchbaseServer = "default";

            settings.sqlServers = new List<SQLServer>();
            settings.sqlServers.Add(new SQLServer()
            {
                id = "default",
                connectionString = "Server=localhost;"
            });

            settings.primarySqlServer = "default";

            settings.contentStorages = new List<StorageLocation>();
            settings.contentStorages.Add(
                new StorageLocation()
                {
                    id = "default",
                    path = "D:\\_contents"
                }
            );

            settings.primaryContentStorage = "default";

            settings.applicationStorages = new List<StorageLocation>();
            settings.applicationStorages.Add(
                new StorageLocation()
                {
                    id = "default",
                    path = "D:\\_webs"
                }
            );

            settings.primaryApplicationStorage = "default";

            string serialized = Newtonsoft.Json.JsonConvert.SerializeObject(settings, Newtonsoft.Json.Formatting.Indented);
        }
    }
}
