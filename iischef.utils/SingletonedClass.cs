﻿using System;
using System.Runtime.CompilerServices;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Web.UI;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace iischef.utils
{
    public abstract class SingletonedClass<TSingletonedClass>
        where TSingletonedClass : class, new()
    {
        #region Gestión del Singleton

        //Almacen del singleton de acceso
        private static TSingletonedClass _Instance = null;
		private static bool Initializing = false;

        static readonly object padlock = new object();

        // creador sincronizado para protegerse de posibles problemas multi-hilo
        // otra prueba para evitar instanciación múltiple 
        [MethodImpl(MethodImplOptions.Synchronized)]
        private static void CreateInstance()
        {
            _Instance = new TSingletonedClass();
        }

		protected void ResetInstance()
		{
			_Instance = null;
		}


        //Obtiene la instancia actual del singleton
        public static TSingletonedClass Instance
        {
            get
            {
                if (Initializing)
                {
                    //En escenarios concurrentes es posible que inicialicemos en paralelo. Es por ello
                    //que intentaremos esperar un poco antes de lanzar la excepción.

                    int maxwait = 250; //Tiempo entre pausas
                    int numpauses = 3; //Número máximo de esperas o pausas

                    for (int pause = 0; pause < numpauses; pause++)
                    {
                        System.Threading.Thread.Sleep(maxwait);
                        if (!Initializing)
                        { 
                            //Bingo!! la espera ha tenido sus frutos: la instancia se ha creado
                            break;
                        }
                    }

                    if(Initializing)
                        throw new Exception("CE: Cannot acces singletoned instance while class is being instanced");
                }

				Exception exGeneratedException = null;

                if (_Instance == null)
                {
					lock (padlock)
					{
						if (_Instance == null)
						{
							try
							{
								Initializing = true;
								CreateInstance();
							}
							catch (Exception ex)
							{
								_Instance = null;
								exGeneratedException = ex;
							}
							finally
							{
								Initializing = false;
							}
						}
					}

					if(exGeneratedException != null)
						throw exGeneratedException;

					return _Instance;

                }
                return _Instance;
            }
        }

        #endregion
    }
}
