﻿using CERTENROLLLib;
using System;
using System.IO;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;

namespace iischef.utils
{
    /// <summary>
    /// 
    /// </summary>
    public static class UtilsCertificate
    {
        /// <summary>
        /// Generate a PFX file
        /// </summary>
        /// <param name="crtFile"></param>
        /// <param name="keyFile"></param>
        /// <param name="pfxFile"></param>
        /// <param name="certificateFilePassword"></param>
        [MethodImpl(MethodImplOptions.NoInlining)]
        public static void CreatePfXfromPem(
            string crtFile,
            string keyFile,
            string pfxFile,
            string certificateFilePassword)
        {
            string dat;
            using (StreamReader stream = new StreamReader(crtFile))
            {
                dat = stream.ReadToEnd();
            }

            OpenSSL.Core.BIO certbio = new OpenSSL.Core.BIO(dat);
            OpenSSL.X509.X509Certificate x = new OpenSSL.X509.X509Certificate(certbio);

            string datakey;

            using (StreamReader stream = new StreamReader(keyFile))
            {
                datakey = stream.ReadToEnd();
            }

            OpenSSL.Crypto.CryptoKey key = OpenSSL.Crypto.CryptoKey.FromPrivateKey(datakey, certificateFilePassword);
            OpenSSL.Core.BIO a = OpenSSL.Core.BIO.File(pfxFile, "wb");
            OpenSSL.Core.Stack<OpenSSL.X509.X509Certificate> ca = new OpenSSL.Core.Stack<OpenSSL.X509.X509Certificate>();
            OpenSSL.X509.PKCS12 p12 = new OpenSSL.X509.PKCS12(certificateFilePassword, key, x, ca);

            p12.Write(a);

            key.Dispose();
            p12.Dispose();
            a.Dispose();
        }

        /// <summary>
        /// TODO: Asegurar que no saturamos las certificaciones locales...
        /// </summary>
        [MethodImpl(MethodImplOptions.NoInlining)]
        public static void RemoveCertificateFromLocalStore(string thumbPrint)
        {
            X509Store store = new X509Store(StoreName.My, StoreLocation.LocalMachine);
            store.Open(OpenFlags.ReadWrite | OpenFlags.IncludeArchived);

            // You could also use a more specific find type such as X509FindType.FindByThumbprint
            X509Certificate2Collection col =
                store.Certificates.Find(X509FindType.FindByThumbprint, thumbPrint, false);

            foreach (var cert in col)
            {
                // Remove the certificate
                store.Remove(cert);
            }

            store.Close();
        }

        /// <summary>
        /// Find certificate in store
        /// </summary>
        /// <param name="thumbPrint"></param>
        /// <param name="storeName"></param>
        /// <param name="storeLocation"></param>
        /// <returns></returns>
        public static X509Certificate2 FindCertificate(string thumbPrint, StoreName storeName, StoreLocation storeLocation)
        {
            X509Certificate2 certificate = null;

            X509Store store = new X509Store(storeName, storeLocation);
            store.Open(OpenFlags.ReadOnly | OpenFlags.IncludeArchived);

            // You could also use a more specific find type such as X509FindType.FindByThumbprint
            X509Certificate2Collection col =
                store.Certificates.Find(X509FindType.FindByThumbprint, thumbPrint, false);

            foreach (var cert in col)
            {
                certificate = cert;
            }

            store.Close();

            return certificate;
        }

        /// <summary>
        /// Create a self signed certificate and enroll it to local store
        /// </summary>
        /// <param name="subjectName"></param>
        /// <param name="friendlyName"></param>
        /// <param name="expirationDays"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.NoInlining)]
        public static X509Certificate2 CreateSelfSignedCertificate(
            string subjectName,
            string friendlyName,
            int expirationDays = 90)
        {
            // create DN for subject
            var dn = new CX500DistinguishedName();
            dn.Encode("CN=" + subjectName, X500NameFlags.XCN_CERT_NAME_STR_NONE);

            // create DN for the issuer
            var issuer = new CX500DistinguishedName();
            issuer.Encode("CN=ChefCertificate", X500NameFlags.XCN_CERT_NAME_STR_NONE);

            // create a new private key for the certificate
            CX509PrivateKey privateKey = new CX509PrivateKey();
            privateKey.ProviderName = "Microsoft Base Cryptographic Provider v1.0";
            privateKey.MachineContext = true;
            privateKey.Length = 2048;
            privateKey.KeySpec = X509KeySpec.XCN_AT_SIGNATURE; // use is not limited
            privateKey.ExportPolicy = X509PrivateKeyExportFlags.XCN_NCRYPT_ALLOW_PLAINTEXT_EXPORT_FLAG;
            privateKey.Create();

            // Use the stronger SHA512 hashing algorithm
            var hashobj = new CObjectId();
            hashobj.InitializeFromAlgorithmName(ObjectIdGroupId.XCN_CRYPT_HASH_ALG_OID_GROUP_ID,
                ObjectIdPublicKeyFlags.XCN_CRYPT_OID_INFO_PUBKEY_ANY,
                AlgorithmFlags.AlgorithmFlagsNone, "SHA512");

            // add extended key usage if you want - look at MSDN for a list of possible OIDs
            var oid = new CObjectId();
            oid.InitializeFromValue("1.3.6.1.5.5.7.3.1"); // SSL server
            var oidlist = new CObjectIds();
            oidlist.Add(oid);
            var eku = new CX509ExtensionEnhancedKeyUsage();
            eku.InitializeEncode(oidlist);

            // Create the self signing request
            var cert = new CX509CertificateRequestCertificate();
            cert.InitializeFromPrivateKey(X509CertificateEnrollmentContext.ContextMachine, privateKey, "");
            cert.Subject = dn;
            cert.Issuer = issuer;
            cert.NotBefore = DateTime.Now;

            // this cert expires immediately. Change to whatever makes sense for you
            cert.NotAfter = DateTime.Now.AddDays(expirationDays);
            cert.X509Extensions.Add((CX509Extension)eku); // add the EKU
            cert.HashAlgorithm = hashobj; // Specify the hashing algorithm
            cert.Encode(); // encode the certificate

            // Do the final enrollment process
            var enroll = new CX509Enrollment();
            enroll.InitializeFromRequest(cert); // load the certificate
            enroll.CertificateFriendlyName = friendlyName; // Optional: add a friendly name
            string csr = enroll.CreateRequest(); // Output the request in base64

            // and install it back as the response
            enroll.InstallResponse(
                InstallResponseRestrictionFlags.AllowUntrustedCertificate,
                csr,
                EncodingType.XCN_CRYPT_STRING_BASE64, ""); // no password

            // output a base64 encoded PKCS#12 so we can import it back to the .Net security classes
            var base64Encoded = enroll.CreatePFX(
                string.Empty, // no password, this is for internal consumption
                PFXExportOptions.PFXExportChainWithRoot);

            // instantiate the target class with the PKCS#12 data (and the empty password)
            return new X509Certificate2(
                Convert.FromBase64String(base64Encoded),
                string.Empty,
                // mark the private key as exportable (this is usually what you want to do)
                X509KeyStorageFlags.Exportable
            );
        }
    }
}
