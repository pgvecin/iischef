﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace iischef.utils
{
    public class UtilsJson
    {
        /// <summary>
        /// Get an associative array from a JOBJECT or JARRAY. Prepopulates the ID
        /// property of the object (in case it does not exist) with the associative array key.
        /// </summary>
        /// <returns></returns>
        public static Dictionary<string, JToken> keyedFromArrayOrObject(JToken source)
        {
            Dictionary<string, JToken> result = new Dictionary<string, JToken>();

            // Our custom deserialization already has support for propagating ID information from regular arrays to
            // associative arrays.
            return UtilsJson.DeserializeObject<Dictionary<string, JToken>>(source.ToString());
        }

        public static TType DeserializeObject<TType>(string value)
        {
            return (TType)DeserializeObject(value, typeof(TType));
        }

        /// <summary>
        /// Deserializes from JSON converting all JARRAYS to associative
        /// arrays...
        /// </summary>
        /// <typeparam name="TType"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static object DeserializeObject(string value, Type targetType)
        {
            using (var strReader = new StringReader(value))
            {
                using (var jsonReader = new CustomJsonTextReader(strReader))
                {
                    var resolver = new CustomContractResolver();
                    var serializer = new CustomJsonSerializer { ContractResolver = resolver, ObjectCreationHandling = ObjectCreationHandling.Replace };
                    object unserialized = serializer.Deserialize(jsonReader, targetType);
                    return unserialized;
                }
            }
        }
    }

    public class CustomJsonSerializer : JsonSerializer
    {



    }

    public class CustomJsonTextReader : JsonTextReader
    {

        public CustomJsonTextReader(TextReader textReader) : base(textReader) { }

        public override bool Read()
        {
            return base.Read();
        }

    }

    public class CustomContractResolver : DefaultContractResolver
    {

        public bool skip = false;

        public CustomContractResolver()
        {

        }

        public override JsonContract ResolveContract(Type type)
        {
            var contract = base.ResolveContract(type);
            // Very careful here... contract resolvers and converters are stored in an internal
            // cache.... so ResolveContractConverter() will not always get called once
            // the resolution is cached. What we are going to do is override the converter
            // very time.
            contract.Converter = ResolveContractConverter(type);

            if (skip)
            {
                contract.Converter = null;
                skip = false;
            }

            return contract;
        }

        protected override JsonArrayContract CreateArrayContract(Type objectType)
        {
            return base.CreateArrayContract(objectType);
        }

        protected override JsonProperty CreateProperty(
            MemberInfo member, MemberSerialization memberSerialization)
        {
            var property = base.CreateProperty(member, memberSerialization);
            var shouldSerialize = property.ShouldSerialize;
            property.ShouldSerialize = obj => (shouldSerialize == null ||
                                               shouldSerialize(obj));



            return property;
        }

        protected override JsonConverter ResolveContractConverter(Type objectType)
        {
            var converter = base.ResolveContractConverter(objectType);

            if (typeof(IDictionary).IsAssignableFrom(objectType) && !skip)
            {
                converter = new ArrayToDictionaryConverter();
            }

            return converter;
        }
    }

    /// <summary>
    /// Use KnownType Attribute to match a divierd class based on the class given to the serilaizer
    /// Selected class will be the first class to match all properties in the json object.
    /// </summary>
    public class ArrayToDictionaryConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            // FrameWork 4.5
            // return typeof(T).GetTypeInfo().IsAssignableFrom(objectType.GetTypeInfo());
            // Otherwise
            return typeof(Type).IsAssignableFrom(objectType);
        }

        public override bool CanWrite
        {
            get { return false; }
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            bool isdictionary = typeof(IDictionary).IsAssignableFrom(objectType);

            CustomContractResolver resolver = (CustomContractResolver)serializer.ContractResolver;
            resolver.skip = true;

            if (isdictionary && reader.TokenType != JsonToken.StartObject)
            {
                IDictionary result = (IDictionary)System.Activator.CreateInstance(objectType);

                Type[] arguments = result.GetType().GetGenericArguments();
                Type keyType = arguments[0];
                Type valueType = arguments[1];

                var listedType = typeof(List<>).MakeGenericType(valueType);

                // This will loop in itself badly...
                IEnumerable temp = (IEnumerable)serializer.Deserialize(reader, listedType);

                int pos = 0;
                foreach (var o in temp)
                {
                    // We "might" be able to get the keys from the ID's themselves
                    // to provide backwards compatibility...
                    string key = setOrGetIdValue(pos.ToString(), o);
                    if (result.Contains(key))
                    {
                        throw new Exception("CE: Error while converting array to associative map. Possible duplicated ID's in array definition.");
                    }
                    result.Add(key, o);
                    pos++;
                }

                return result;
            }
            else
            {
                // This will loop in itself badly...
                IDictionary result = (IDictionary)serializer.Deserialize(reader, objectType);

                // Now populate ID's with keys when possible...
                foreach (string key in result.Keys)
                {
                    setOrGetIdValue(key, result[key], true);
                }

                return result;
            }

            throw new Exception("CE: Unsupported operation.");
        }

        /// <summary>
        /// Will try to set an ID, or try to retrieve an existing one....
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        protected string setOrGetIdValue(string key, object value, bool onlyset = false)
        {

            var prop = value.GetType().GetProperty("id");

            if (prop == null)
            {
                prop = value.GetType().GetProperty("Id");
            }

            if (prop != null && prop.PropertyType == typeof(String))
            {
                string currentvalue = (String)prop.GetValue(value);
                if (string.IsNullOrWhiteSpace(currentvalue))
                {
                    prop.SetValue(value, key);
                    return key;
                }
                else
                {
                    return currentvalue;
                }
            }

            if (value is JToken || value is JObject)
            {
                string id = (string)((JToken)value)["id"];
                if (id == null)
                {
                    id = (string)((JToken)value)["Id"];
                }

                if (String.IsNullOrWhiteSpace(id))
                {
                    ((JToken)value)["id"] = key;
                    return key;
                }
                else
                {
                    return id;
                }
            }

            if (!onlyset)
            {
                throw new Exception("Failure in JSON id array-to-key conversion.");
            }

            return null;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}
