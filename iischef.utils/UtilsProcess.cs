﻿using Microsoft.Win32;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace iischef.utils
{
    public static class UtilsProcess
    {
        /// <summary>
        /// Devuelve un listado con todos los procesos que tienen
        /// algun fichero bloqueado de manera directa en el path 
        /// del parámetro.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static List<Process> GetPathProcesses(string path)
        {
            // El método de threads no da todos los detalles de proceso, utilizaremos primero
            // la utilidad handle de SysInternals para sacar una preview.
            var pids = GetProcessesThatBlockPathHandle(path);

            ConcurrentBag<Process> myProcessArray = new ConcurrentBag<Process>();

            List<Process> processlist = Process.GetProcesses().ToList();

            var ops = new ParallelOptions();
            ops.MaxDegreeOfParallelism = 3;

            Parallel.ForEach(processlist, ops, (myProcess) =>
            {
                // There are randmo exceptions such as the process exited
                // between the listing and actually operating on it.
                try
                {

                    if (pids.Contains(myProcess.Id))
                    {
                        myProcessArray.Add(myProcess);
                        return;
                    }

                    //if (!myProcess.HasExited) //This will cause an "Access is denied" error
                    if (myProcess.Threads.Count > 0)
                    {
                        try
                        {
                            ProcessModuleCollection modules = myProcess.Modules;
                            for (var j = 0; j <= modules.Count; j++)
                            {
                                if ((modules[j].FileName.ToLower().IndexOf(path.ToLower()) != -1))
                                {
                                    myProcessArray.Add(myProcess);
                                    return;
                                }
                            }
                        }
                        catch (Exception exception)
                        {
                            //MsgBox(("Error : " & exception.Message)) 
                        }
                    }

                }
                catch (Exception e)
                {
                    return;
                }
            });

            return myProcessArray.ToList();
        }

        /// <summary>
        /// Muestra en consola información sobre los pools de PHP abiertos, y el número de procesos
        /// de cada uno.
        /// </summary>
        public static void OutputProcessData()
        {
            // Obtener un diccionario con los nombres de AppPools y el número de procesos asociados
            Dictionary<string, List<Process>> inventory = new Dictionary<string, List<Process>>();

            Process[] processlist = Process.GetProcesses();
            for (int x = 0; x < processlist.Count(); x++)
            {
                try
                {
                    if (processlist[x].MainModule.ModuleName == "php-cgi.exe")
                    {
                        Console.WriteLine("Process: {0} ID: {1}", processlist[x].ProcessName, processlist[x].Id);
                        var owner = processlist[x].WindowsIdentity().Name;
                        var pool = System.Text.RegularExpressions.Regex.Split(owner, "\\\\").Last();
                        if (!inventory.ContainsKey(pool))
                        {
                            inventory.Add(pool, new List<Process>());
                        }
                        inventory[pool].Add(processlist[x]);
                    }
                }
                catch { }
            }

            foreach (var site in inventory)
            {
                //Logger.Instance.Log("Application Pool {0} has {1} running proceses.", LoggerSeverity.Normal, site.Key, site.Value.Count.ToString());
            }
        }


        /// <summary>
        /// Cierra los procesos que pertenezcan a un apppool concreto.
        /// </summary>
        public static void ClosePathProcesses(string path)
        {
            if (string.IsNullOrWhiteSpace(path))
            {
                return;
            }

            // OJO, SI ESTO NO ESTÁ ASÍ EL SISTEMA PUEDE PETAR!!
            if (!File.Exists(path) && !Directory.Exists(path))
            {
                return;
            }

            var process = GetPathProcesses(path);

            List<Process> KilledProcesses = new List<Process>();

            // First kill al the processes.
            foreach (var p in process)
            {
                try
                {
                    //Logger.Instance.Log("Killing process: {0}", p.MainModule.ModuleName);
                    p.Kill();
                    KilledProcesses.Add(p);
                }
                catch { }
            }

            // Habria que programar un algoritmo de espera real...
            var max_wait = 30;
            var start = DateTime.Now;
            //Logger.Instance.Log("Waiting for processes to stop (max {0}s)", max_wait.ToString());
            foreach (var p in KilledProcesses)
            {
                while (true)
                {
                    if ((DateTime.Now - start).TotalSeconds > max_wait)
                    {
                        break;
                    }

                    if (p.HasExited)
                    {
                        //Logger.Instance.Log("1 Process confirmed as exited.");
                        break;
                    }

                    System.Threading.Thread.Sleep(1000);
                    //Logger.Instance.Log("Wating...");
                }

                if ((DateTime.Now - start).TotalSeconds > max_wait)
                {
                    break;
                }
            }

        }

        public static object GetRegistryKeyValue32(RegistryHive hive, string key, string value, object defaultValue)
        {
            var view32 = RegistryKey.OpenBaseKey(hive,
                                     RegistryView.Registry32);

            using (var clsid32 = view32.OpenSubKey(key, false))
            {
                if (clsid32 == null)
                {
                    return defaultValue;
                }

                // actually accessing Wow6432Node 
                return clsid32.GetValue(value);
            }
        }

        public static object GetRegistryKeyValue64(RegistryHive hive, string key, string value, object defaultValue)
        {
            var view64 = RegistryKey.OpenBaseKey(hive,
                                     RegistryView.Registry64);

            using (var clsid64 = view64.OpenSubKey(key, false))
            {
                if (clsid64 == null)
                {
                    return defaultValue;
                }

                // actually accessing Wow6432Node 
                return clsid64.GetValue(value);
            }
        }

        public static void setRegistryValue(string key, string name, object realValue)
        {

            var view32 = RegistryKey.OpenBaseKey(RegistryHive.CurrentUser,
                                     RegistryView.Registry32);

            using (var clsid32 = view32.OpenSubKey(key, true))
            {
                // actually accessing Wow6432Node
                if (clsid32 != null)
                {
                    clsid32.SetValue(name, realValue);
                }
                else
                {
                    using (var k = view32.CreateSubKey(key))
                    {
                        k.SetValue(name, realValue);
                    }
                }
            }

            var view64 = RegistryKey.OpenBaseKey(RegistryHive.CurrentUser,
                         RegistryView.Registry64);

            using (var clsid64 = view64.OpenSubKey(key, true))
            {
                // actually accessing Wow6432Node 
                if (clsid64 != null)
                {
                    clsid64.SetValue(name, realValue);
                }
                else
                {
                    using (var k = view64.CreateSubKey(key))
                    {
                        k.SetValue(name, realValue);
                    }
                }
            }
        }

        private static List<int> GetProcessesThatBlockPathHandle(string path)
        {
            if (!File.Exists(path) && !Directory.Exists(path))
            {
                return new List<int>();
            }

            string key = "SOFTWARE\\Sysinternals\\Handle";
            string name = "EulaAccepted";

            // This Utility has an EULA GUI on first run... try to avoid that
            // by manually setting the registry
            int? eulaaccepted64 = (int?)GetRegistryKeyValue64(RegistryHive.CurrentUser, key, name, null);
            int? eulaaccepted32 = (int?)GetRegistryKeyValue32(RegistryHive.CurrentUser, key, name, null);

            bool eulaaccepted = (eulaaccepted32 == 1 && eulaaccepted64 == 1);

            if (!eulaaccepted)
            {
                setRegistryValue(key, name, 1);
            }

            string fileName = path;//Path to locked file
            List<int> result = new List<int>();
            string outputTool;

            using (Process tool = new Process())
            {
                // Gather the handle.exe from the embeded resource and into a temp file
                var handleexe = UtilsSystem.GetTempPath("handle_" + (Guid.NewGuid().ToString()) + ".exe");

                UtilsSystem.EmbededResourceToFile(Assembly.GetExecutingAssembly(), "_Resources.Handle.exe", handleexe);

                tool.StartInfo.FileName = handleexe;
                tool.StartInfo.Arguments = fileName;
                tool.StartInfo.UseShellExecute = false;
                tool.StartInfo.RedirectStandardOutput = true;
                tool.Start();

                outputTool = tool.StandardOutput.ReadToEnd();

                try
                {
                    tool.Close();
                    tool.Dispose();
                    tool.Kill();
                }
                catch
                {
                }

                File.Delete(handleexe);
            }

            string matchPattern = @"(?<=\s+pid:\s+)\b(\d+)\b(?=\s+)";
            foreach (Match match in Regex.Matches(outputTool, matchPattern))
            {
                if (!match.Value.IsInt32())
                    continue;

                if (!result.Contains(match.Value.ToInt32()))
                {
                    result.Add(match.Value.ToInt32());
                }
            }

            return result;
        }
    }
}
